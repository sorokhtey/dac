#!/usr/bin/env python

import argparse
import glob
import logging as log
import os
import re
import sys
import time
from collections import defaultdict
from concurrent.futures import ThreadPoolExecutor
from xml.etree import ElementTree as Et

import requests
from requests.packages.urllib3.exceptions import MaxRetryError

import dac

"""
Markdown content checker.

* product misspellings
* dead links
* blacklisted internal atlassian hosts in urls
* markdown structure checks
"""

LOG_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'

"""
Regexes used to match URLs that we want to exclude from the list we do an HTTP GET on. Mostly examples, localhost
etc. We should use example URL conventions across the docs such as something.example.com
"""
NOCHECK_LINKS = [
    # we don't want to check the dac_edit_link or dac_view_link in the frontmatter, sometimes they link to hidden
    # content
    r'https://developer\.atlassian\.com/pages/(view|edit)page\.action\?cjm=wozere.*',
    r'https?://example-dev1\.atlassian\.net.*',
    r'.*\.ngrok\.(io|com).*',
    r'.*\.amazon\.com.*',
    r'https://registry.npmjs.org/{package_name}.*',
    r'http://maven.apache.org/POM/4\.0\.0',
    r'https?://\d+\.\d+\.\d+\.\d+.*',
    r'.*\?\S*access-token=\S+.*',
    r'.*(\w+\.)?gravatar.com.*',
    r'.*(\w+\.)?giphy.com.*[}{]',
    r'.*giphy.com.*',
    r'.*api\.hipchat\.com.*',
    r'.*www\.myremoteapplication\.com.*',
    r'.*www.google.com/cse/cse.js.*',
    r'https://marketplace\.atlassian\.com/rest/.*',
    r'.*my-site\.atlassian\.net.*',
    r'.*jira-instance1\.net.*',
    r'https?://BASE_URL\.atlassian\.net.*',
    r'https?://HOSTNAME\.atlassian\.net.*',
    r'https://mycompany\.atlassian\.net/.*',
    r'.*(\w+\.)?example\.com.*',
    r'.*example\.atlassian\.net.*',
    r'.*myhost\.local(?:\d+)?.*',
    r'https?://marketplace\.atlassian\.com/plugins/\[your-add-on-key\]',
    r'.*host:port.*',
    r'.*127.0.0.1.*',
    r'.*\{\{.*\}\}.*',
    r'.*localhost(:\d+)?.*'
]

OK_HTTP_CODES = [200, 302, 401, 403, 429, 451]

URL = re.compile(r'(?<!:)(https?:)?//([^<>?;/\s#.`*)]+)\.([^<>;/\s#\][()]+)([^\s"\'<>#`)(\][]+)([^."\'\s)<>#\][)(`*\\])', re.I | re.S)


class TestCase:
    """
    Abstract representation of a test case result to the build system. An empty
    list of violations is a success result.
    """
    def __init__(self, name, desc):
        """
        :param name: short, unique identifier
        :param desc: describes the check being made
        """
        super().__init__()
        self.name = name
        self.desc = desc
        self.violations = []

    def fail(self, violation):
        log.info("FAIL: " + violation)
        self.violations.append(violation)

    def is_success(self):
        return len(self.violations) == 0

    def is_failure(self):
        return not self.is_success()


class PatternTest(TestCase):
    def __init__(self, name, desc, regex, flags=re.I | re.S):
        """
        :param regex: the regex to match markdown content
        :param flags: optional flags for the regex find
        """
        super().__init__(name, desc)
        self.regex = regex
        self.flags = flags

    def finditer(self, markdown):
        return re.finditer(self.regex, markdown, self.flags)

    def test(self, file, markdown):
        for m in self.finditer(markdown):
            # match => fail
            if m:
                line_number = markdown[:m.start()].count('\n') - 1
                self.fail("Found '%s' in %s line %d " % (m.group(0), file, line_number))


class LinkCheckTest(TestCase):
    def __init__(self, link, files):
        super().__init__("Check %s" % link, "Fetch and verify a successful response from %s" % link)
        self.link = link
        self.files = files

    def test(self):
        success, mesg = fetch_link(self.link)
        success or self.fail("%s linked from %s" % (mesg, ", ".join(self.files)))


def ok_to_check(link):
    """
    Determines if the link should be fetched and verified. Links matching the
    blacklist patterns (e.g. example.com) do not need to be verified.
    :param link: the link to check.
    :return: true only if the link should be verified.
    """
    return not next(filter(lambda r: re.match(r, link), NOCHECK_LINKS), None)


def collect_links(md):
    """
    Returns a list of links found in the given markdown text. Finds both markdown links and html a hrefs.
    :param md: some markdown
    :return: a list of URLs
    """
    links = []
    for link_match in re.finditer(URL, md):
        link = link_match.group(0)
        if not link_match.group(1):
            # handle schemaless URLs, defaulting to non ssl
            link = "http:%s" % link
        links.append(link)
    return links


def fetch_link(link):
    """
    Does an http get request on the link.
    :param link: the link to fetch
    :return: a tuple of (success, message)
    """
    response_code = "not attempted"
    try:
        # configured to follow redirects like a browser
        response_code = requests.get(link).status_code
        mesg = "response code %s for %s" % (response_code, link)
    except requests.exceptions.ConnectionError as err:
        mesg = "http connection failed on %s: %s" % (link, err)
    except MaxRetryError as err:
        mesg = "http request failed after many retries on %s: %s" % (link, err)
    except Exception as err:
        mesg = "http request failed on %s: %s" % (link, err)

    success = response_code in OK_HTTP_CODES
    if success:
        log.debug(mesg)
    else:
        log.warning(mesg)
    return success, mesg


def fail_whale(file, ex):
    log.error("Fail on %s because %s", file, ex)


def for_markdown_tree(root, fn, exception_handler=fail_whale):
    """
    Do fn(file, contents) for each markdown file in the dir tree rooted at root, calling exception_handler
    if asplode.
    :param root: root dir for markdown processing
    :param fn: to be called for each markdown, i.e. fn(file, contents)
    :param exception_handler: maybe called like g(file, exception)
    """
    for f in glob.glob(root + '/**/*.md', recursive=True):
        log.info("Checking %s", f)
        with open(f, encoding="utf-8") as infile:
            try:
                fn(f, infile.read())
            except Exception as e:
                # file read problems (or if fn raises)
                exception_handler(f, e)


def run_markdown_tests(root, regex_suite):

    def run_tests(f, markdown):
        for case in regex_suite:
            try:
                case.test(f, markdown)
            except Exception as e:
                case.fail("exception caught file %s: %s" % (f, e))

    for_markdown_tree(root, run_tests)


# noinspection PyTypeChecker
def write_xml_test_results(results, elapsed_time, xml_dir='test-reports'):
    # basic format of junit xml file:
    # <testsuite errors="0" failures="0" name="pytest" skips="0" tests="13" time="0.227">
    #   <testcase classname="" file="" ... />
    #   ...
    # </testsuite>

    log.info("writing XML error output")
    root = Et.Element('testsuite')
    root.set('skips', "0")
    root.set('errors', "0")
    fail_count = len([r for r in results if r.is_failure()])
    test_count = len(results)
    root.set('tests', str(test_count))
    root.set('failures', str(fail_count))
    root.set('name', "lint")
    root.set('time', str(elapsed_time))
    for test in results:
        testcase = Et.SubElement(root, 'testcase', {'classname': type(test).__name__, 'name': test.name})
        if test.is_failure():

            if hasattr(test, 'files'):
                for v in test.violations:
                    failure = Et.SubElement(testcase, 'failure', {'type': 'linked-from-file'})
                    failure.text = v
            else:
                for v in test.violations:
                    failure = Et.SubElement(testcase, 'failure', {'type': 'found-in-file'})
                    failure.text = v

    xml_file = os.path.join(xml_dir, "content-lint-tests.xml")
    tree = Et.ElementTree(root)
    tree.write(xml_file, xml_declaration=True, encoding='utf-8')


def create_link_checker_suite(root):

    links_and_the_files_who_love_them = collect_file_links(root)

    suite = []
    for link, files in links_and_the_files_who_love_them.items():
        suite.append(LinkCheckTest(link, files))

    return suite


def collect_file_links(root):
    """
    Build a multimap of unique links found in the markdown file tree rooted at root and the list of files
    that refer to each such link.
    :param root: a local dir in which to ferret out markdown files.
    :return: a map of link to list of file
    """
    links_and_the_files_who_love_them = defaultdict(list)  # multimap

    def file_links_collector(f, markdown):
        links = collect_links(markdown)
        for l in links:
            if ok_to_check(l):
                links_and_the_files_who_love_them[l].append(f)

    for_markdown_tree(root, file_links_collector)
    return links_and_the_files_who_love_them


def run_link_checker_tests(link_checker_suite):
    with ThreadPoolExecutor(max_workers=50) as executor:
        for case in link_checker_suite:
            executor.submit(case.test)
        executor.shutdown(wait=True)


def create_regex_suite():

    curly_quotes = "Do not use curly/sided quotes, prefer ' for single or \" for double (Hugo converts)"
    regex_suite = [
        PatternTest("JIRA spelling", "JIRA spelt 'JRIA'", r'\bjria\b'),
        PatternTest("JIRA case", "JIRA written as 'Jira'", r'\bJira\b', re.S),
        PatternTest("Bitbucket case", "Bitbucket written as 'BitBucket'", r'\bBitBucket\b', re.S),
        PatternTest("HipChat case", "HipChat written as 'Hipchat'", r'\bHipchat\b', re.S),
        PatternTest("Windows carriage return", r'Wrong line ending, "\r", only use "\n"', r'\r', re.S),
        PatternTest("Illegal non-breaking space", "Don't use NBSP unicode chars, prefer &nbsp;", dac.CHAR_NBSP),
        PatternTest("Unicode em dash", "Use -- instead of actual em dash, Hugo will convert it.", dac.CHAR_EM_DASH),
        PatternTest("Unicode en dash", "Use &ndash; instead of actual unicode en dash.", dac.CHAR_EN_DASH),
        # sided quotes mess up markdown parser, hugo creates smartquotes from ' and "
        PatternTest("Unicode right double quote", curly_quotes, dac.CHAR_RIGHT_DOUBLE_QUOTE),
        PatternTest("Unicode left double quote", curly_quotes, dac.CHAR_LEFT_DOUBLE_QUOTE),
        PatternTest("Unicode right single quote", curly_quotes, dac.CHAR_RIGHT_SINGLE_QUOTE),
        PatternTest("Unicode left single quote", curly_quotes, dac.CHAR_LEFT_SINGLE_QUOTE),
        PatternTest("Markdown heading convention",
                  "hyphens or equals underlined headings, prefer hash prefix", r'\S\n[-=]{4,}\n\n'),
        PatternTest("Internal hosts", "Don't link to internal Atlassian hosts in public content",
                  r'(staging\.atlassian\.(com|io)|pug\.jira(-dev)?\.com)'),
        PatternTest("More internal hosts", "No internal host linking, yo.", r'(extranet|internal)\.atlassian\.com')

    ]
    return regex_suite


def main(root, xml_path, skip_http):
    start_time = time.time()
    regex_suite = create_regex_suite()
    run_markdown_tests(root, regex_suite)

    all_tests = []
    all_tests.extend(regex_suite)

    if not skip_http:
        link_checker_suite = create_link_checker_suite(root)
        log.info("links to check: %s", len(link_checker_suite))
        run_link_checker_tests(link_checker_suite)
        all_tests.extend(link_checker_suite)

    elapsed_time = time.time() - start_time

    fails = [case for case in all_tests if case.is_failure()]
    # noinspection PyTypeChecker
    error_count = len(fails)
    if error_count > 0:
        log.info("Your shipment of fail has arrived.")
        for t in fails:
            log.error(str(t.name) + " " + str(t.desc))

    write_xml_test_results(all_tests, elapsed_time, xml_path)
    log.info("%d %s of error found", error_count, "type" if error_count == 1 else "types")
    return 0


def get_options():
    parser = argparse.ArgumentParser(description='Run content checks on all markdown files in a root dir.')
    parser.add_argument('root', metavar='root', help='root dir')
    parser.add_argument('-x', '--xml-test-output', dest="xml",
                        default="test-reports", help='verbose: where the xml report should go')
    parser.add_argument('-v', '--verbose', action='store_true',
                        help='verbose: print out debug information')
    parser.add_argument('-s', '--skip-http', dest="skip_http", action='store_true',
                        help='skip-http: do not check links')
    options = parser.parse_args()
    log.basicConfig(level=log.DEBUG if options.verbose else log.INFO, format=LOG_FORMAT)
    return options


if __name__ == '__main__':
    opts = get_options()
    sys.exit(main(opts.root, opts.xml, opts.skip_http))
