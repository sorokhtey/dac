---
title: Displaying in the Quick Navigation Search 39988673
aliases:
    - /confcloud/displaying-in-the-quick-navigation-search-39988673.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39988673
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39988673
confluence_id: 39988673
platform:
product:
category:
subcategory:
---
# Confluence Connect : Displaying in the quick navigation search

## Graphic elements required

An icon of your content type with the following specs:

-   16x16px in size, optimised for retina display
-   Transparent background
-   Line weight of 1px for icon graphics
-   Colour \#707070 only

If no icon is provided, we'll use the default object attachment icon ![].

Confluence uses your icon in the quick search results so users can easily identify different content types.

![][1]

## When will this icon be used?

Confluence uses your icon in the quick navigation search, which appears as a dropdown from the header. Having a distinctive icon helps distinguish your custom content, particularly when other types of content may have a similar title.

## Recommendation

-   Do not use coloured icons or excessive branding.
-   Use only simple graphics.

  []: /confcloud/attachments/39988673/39988776.png
  [1]: /confcloud/attachments/39988673/39989502.png

