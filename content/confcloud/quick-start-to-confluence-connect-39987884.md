---
title: Quick Start to Confluence Connect 39987884
aliases:
    - /confcloud/quick-start-to-confluence-connect-39987884.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39987884
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39987884
confluence_id: 39987884
platform:
product:
category:
subcategory:
---
# Confluence Connect : Quick start to Confluence Connect

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Description</td>
<td>An accelerated guide to building a basic &quot;hello world&quot; macro using Confluence Connect.</td>
</tr>
<tr class="even">
<td>Level</td>
<td><div class="content-wrapper">
1 - QUICK START
</div></td>
</tr>
<tr class="odd">
<td>Estimated Time</td>
<td>10 minutes</td>
</tr>
</tbody>
</table>

Without further ado, let's jump into it!

Okay, so you've decided that you'd like to start building amazing add-ons for Confluence. First of all, **welcome to the community**! We're excited to see what great ideas you bring to our evolving Marketplace. 

# Prerequisites

Ensure you have installed all the tools you need for Confluence Connect add-on development, and running Confluence by going through the [Development setup].

# Hello World!

Awesome, we're now ready to begin building!

Over the next few minutes, we will be doing the following things: 

-   Cloning a repository to create a simple 'hello-world' macro. 
-   Editing the plugin descriptor of this sample add-on to also build a 'giphy' macro. 

Let's get straight into it. 

# The Cloning Zone

To grab the source code for your first Confluence add-on, issue the following command on any terminal application:

``` javascript
my-awesome-machine $ git clone https://bitbucket.org/atlassian/confluence-helloworld-addon  
```

 

Now, access the add-on directory:

``` javascript
my-awesome-machine $ cd confluence-helloworld-addon
```

 

Nearly there! Install all dependencies for the add-on:

``` javascript
my-awesome-machine $ npm install
```

 

Before firing up the server, ensure you have a **credentials.json **file in your add-on directory which links your add-on to your Confluence instance. It should look like this: 

``` javascript
{
    "hosts" : {
        "<your-confluence-domain>/wiki": {
            "username" : "<username>",
            "password" : "<password>"
        }
    }
}
```

 

Now, start the server!

``` javascript
my-awesome-machine $ npm start
```

 

You should see output like the following: 

``` javascript
my-awesome-machine $ npm start
> helloworld-addon@0.0.1 start /Users/mjensen/src/helloworld-confluence-addon
> node app.js

Watching atlassian-connect.json for changes
Initialized memory storage adapter
Add-on server running at http://<your-machine-name>:3000
Local tunnel established at https://1274378e.ngrok.io/
Check http://127.0.0.1:4040 for tunnel status
Registering add-on...
Registered with host Confluence:8628122590 at https://<your-confluence-instance>/wiki
GET /atlassian-connect.json 200 8.299 ms - 783
[VERIFY] Attempt 1 : Verifying public key for instance https://<your-confluence-instance>/wiki
Saved tenant details for Confluence:8628122590 to database
```

 

Awesome! You've just installed your first sample add-on. To see it in action, navigate to any Confluence page, and try the following: 

<img src="/confcloud/attachments/39987884/39987897.png" class="image-center" height="400" />

After selecting this macro and saving your page, you should see content which looks like:

 <img src="/confcloud/attachments/39987884/39987901.png" class="image-center" width="475" />

# GIPHY for one and all

<a href="http://giphy.com" class="external-link">GIPHY</a> is a great online service for finding that GIF you've been looking for to break the ice. 

<img src="/confcloud/attachments/39987884/39987906.png" class="image-center" height="250" />

(Source: <a href="http://giphy.com/gifs/reaction-tobuscus-if-you-know-what-i-mean-p242oyiOmIsX6" class="uri" class="external-link">http://giphy.com/gifs/reaction-tobuscus-if-you-know-what-i-mean-p242oyiOmIsX6</a>)

 

Let's build a quick macro which allows us to achieve this. Add the following module to the **"dyanmicContentMacros"** key in your **atlassian-connect.json** plugin descriptor:

 

``` js
"dynamicContentMacros": [
    //  Hello World macro descriptor...
    {
        "key": "hello-world",
        //  More below here...
    },
    {
        "key": "giphy-macro",
        "name": {
            "value": "Giphy Macro"
        },
        "url": "/gif-renderer?gifUrl={url}&gifWidth={width}&gifHeight={height}",
        "description": {
            "value": "Displays a Gif from Giphy"
            },
        "outputType": "inline",
        "bodyType": "none",
        "featured": true,
        "parameters": [{
            "identifier": "url",
            "name": {
                "value": "URL"
            },
            "description": {
                "value": "Giphy URL."
            },
            "type": "string",
            "required": true
        }, {
            "identifier": "width",
            "name": {
                "value": "Width"
            },
            "type": "string",
            "required": false
        }, {
            "identifier": "height",
            "name": {
                "value": "Height"
            },
            "type": "string",
            "required": false
        }]
    }
]
```

 

Lastly, add the following handler to your **routes/index.js** file.

 

``` js
app.get('/gif-renderer', addon.authenticate(), function (req, res) {
        //  Setup our render parameters.
        var gifUrl    = req.param("gifUrl"),
            gifWidth  = req.param("gifWidth"),
            gifHeight = req.param("gifHeight");
        //  Use Regex to extract the ID.
        var id = extractId(gifUrl);
        // Look up width/height if none are specified
        var getUrl = "http://api.giphy.com/v1/gifs/" + id + "?api_key=dc6zaTOxFJmzC";
        request.get({
            "url": getUrl
        }, function (e, r, body) {      
            var result = JSON.parse(body);
            if (result && result.meta && result.meta.status == 200) {
                var originalHeight = result.data.images.original.height,
                    originalWidth  = result.data.images.original.width;
                //  Either use the original dimensions of the GIF, 
                //  or use passed paramters.
                //  If either of the passed parameters are missing, 
                //  scale it accordingly to retain the correct aspect ratio.
                if (!gifHeight && !gifWidth) {
                    gifWidth = originalWidth;
                    gifHeight = originalHeight;
                } if(gifHeight && !gifWidth) {
                    gifWidth = originalWidth * gifHeight/originalHeight;
                } if(!gifHeight && gifWidth) {
                    gifHeight = originalHeight * gifWidth/originalWidth;
                }
                res.render('gif-renderer', {
                    gifUrl: result.data.images.original.url,
                    gifWidth: gifWidth,
                    gifHeight: gifHeight
                });
            } else {
                res.render('error', {message: "Image not found."});
            }
        });
    }
);
```

 

What have we done here? Let's take a quick look at our descriptor first: 

-   **url:** This url maps our GIPHY macro to the request handler above in **routes/index.js** file. In essence, this url tells our host product which url to hit in our add-on when trying to render our macro. 
-   **parameters:** This array describes all the macro parameters which users can enter when creating the GIPHY macro.

 

Awesome, let's give it a shot!

<img src="/confcloud/attachments/39987884/39987961.png" class="image-center" height="400" />

This will open the macro editor, and allow us to set the parameters described above.

 

<img src="/confcloud/attachments/39987884/39987962.png" class="image-center" height="400" />

 

After populating the macro parameters with valid values, press save and refresh the page. You should see something like this!

 

<img src="/confcloud/attachments/39987884/39987963.png" class="image-center" height="400" />

 

Well done, you've got two basic macros working! This concludes the Quick Start Guide for Confluence Connect. 

# Where to next? 

Check out [Introduction to Confluence Connect] for a more detailed explanation of the above concepts, or head over to the [Confluence Connect patterns] page to start building a complete add-on solution.

 

 

 

  [Development setup]: /confcloud/development-setup-39988911.html
  [Introduction to Confluence Connect]: /confcloud/introduction-to-confluence-connect-39985168.html
  [Confluence Connect patterns]: /confcloud/confluence-connect-patterns-39981569.html

