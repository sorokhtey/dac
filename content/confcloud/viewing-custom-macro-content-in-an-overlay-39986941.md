---
title: Viewing Custom Macro Content in an Overlay 39986941
aliases:
    - /confcloud/viewing-custom-macro-content-in-an-overlay-39986941.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39986941
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39986941
confluence_id: 39986941
platform:
product:
category:
subcategory:
---
# Confluence Connect : Viewing custom macro content in an overlay

In the overlay mode, users can view macro content in detail and perform further actions using the toolbar. This page will help you design a consistent full-screen overlay for custom macro content. 

## Graphic elements required

There are no specific assets required for previewing custom macro content, but you should ensure that the toolbars displayed within the iframe are consistent with the example shown below. The toolbar isn't provided by Connect.

If you're representing actions with icons, use a 1px white outline line style at 16x16 pixels.

*![]*

*Design specification for the toolbar*

## Accessing the preview

Users should be able to access the overlay in either of the following ways:

-   Clicking on a button in your toolbar that appears when they hover over your add-on content
-   Clicking on the add-on content

<img src="/confcloud/attachments/39986941/39986938.gif" width="640" />

 

## UI components

*Toolbar on mouse-over on your add-on content rendered in an iframe*

<img src="/confcloud/attachments/39986941/39986939.png" class="image-center" width="640" />

*Full screen dialog (you'll need to [define your dialog] and use the [Javascript API] to launch it)* 

<img src="/confcloud/attachments/39986941/39986940.png" class="image-center" width="640" />

## Recommendations

-   Have no more than 4 actions for users to take from the toolbar.
-   Use the full screen overlay to show more features of your product.

  []: /confcloud/attachments/39986941/39988812.png
  [define your dialog]: https://developer.atlassian.com/static/connect/docs/latest/modules/common/dialog.html
  [Javascript API]: https://developer.atlassian.com/static/connect/docs/latest/javascript/module-Dialog.html

