---
title: Confluence Xml Rpc and Soap APIs 39985844
aliases:
    - /confcloud/confluence-xml-rpc-and-soap-apis-39985844.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39985844
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39985844
confluence_id: 39985844
platform:
product:
category:
subcategory:
---
# Confluence Connect : Confluence XML-RPC and SOAP APIs

The XML-RPC and SOAP APIs are **deprecated since** **Confluence 5.5**.
Confluence has a new [REST API] that is progressively replacing our existing APIs. We recommend plugin developers use the new REST APIs where possible.

Confluence provides remote APIs in the form of XML-RPC and SOAP interfaces. <a href="http://www.xmlrpc.com/" class="external-link">XML-RPC</a> and SOAP have bindings for almost every language, making them very portable. This document refers to the XML-RPC specification. See [SOAP details below]. 

When choosing between SOAP and XML-RPC you should consider the language for your client and how quickly you need to develop it. SOAP is generally more useful from a strongly typed language (like Java or C\#) but these require more setup. XML-RPC is easier to use from a scripting language (like Perl, Python, or AppleScript) and is often quicker to use.

**On this page:**

-   [Creating an XML-RPC Client]
-   [Creating a SOAP Client]
-   [Remote API version 1 and version 2]
-   [Script Examples]
-   [Changelog]

**See the following child pages:**

-   [Remote Confluence Data Objects]
-   [Remote Confluence Methods]

 

## Creating an XML-RPC Client

Note the following points:

-   The URL for XML-RPC requests is http://*&lt;hostname&gt;*/rpc/xmlrpc
-   All XML-RPC methods must be prefixed by "`confluence2.`" to indicate that this is version 2 of the API. For example to call the `getPage` method, use `confluence2.getPage`. There is also a version 1 API available [see below].
-   All keys in structs are case sensitive.
-   All strings are decoded according to standard XML document encoding rules. Due to a bug in Confluence versions prior to 2.8, strings sent via XML-RPC are decoded using the JVM platform default encoding (<a href="https://jira.atlassian.com/browse/CONF-10213" class="external-link">CONF-10213@JIRA</a>) instead of the XML encoding.
-   Confluence uses 64-bit long values for things like object IDs, but XML-RPC's largest supported numeric type is int32. As a result, all IDs and other long values are converted to Strings when passed through XML-RPC API.
-   For any type identified as a Vector, use the type appropriate for your programming language, such as an Array or List. This is equivalent to the array data type as defined in the XML-RPC specification.
-   For any type identified as a Hashtable, use "Struct" or "Dictionary" or "Map", depending on your programming language. This is equivalent to the struct data type as defined in the XML-RPC specification.
-   The default session lifetime is 60 minutes, but the deployer can change this value by editing the `web.xml` file. This file is located in the following folder: `/confluence/WEB-INF/`

## Creating a SOAP Client

The SOAP API follows the same methods as below, except with typed objects (as SOAP allows for).  To find out more about the SOAP API, simply point your SOAP 'stub generator' at your instance's WSDL file, located at `http://<confluence-install>/rpc/soap-axis/confluenceservice-v2?wsdl`

For reference, you can also view the WSDL file at <a href="http://confluence.atlassian.com/rpc/soap-axis/confluenceservice-v2?wsdl" class="external-link">http://confluence.atlassian.com</a>.

The <a href="https://plugins.atlassian.com/plugin/details/284" class="external-link">Confluence Command Line Interface</a> is a good place to get a functioning client.

## Remote API version 1 and version 2

Confluence 4.0 introduced changes to the way page content is stored in Confluence. Page content is no longer stored as wiki markup, but is now stored in an XHTML based storage format. Confluence 4.0 also introduced a new version 2 remote API. The new version 2 API implements the same methods as the version 1 API, however all content is stored and retrieved using the storage format. This means that you cannot, for example, create a page using wiki markup with the version 2 API, you must instead define the page using the storage format. Atlassian recommends that you migrate towards this new API.

The version 1 remote API will continue to work with Confluence 4.0. You will be able to create pages, blogs and comments in wiki markup using the version 1 API. However you will no longer be able to retrive pages, blogs and comments using the version 1 API.

Specifically the following methods no longer work with the version 1 API:

-   getBlogEntryByDayAndTitle(String token, String spaceKey, int dayOfMonth, String postTitle)
-   getBlogEntryByDateAndTitle(String token, String spaceKey, int year, int month, int dayOfMonth, String postTitle)
-   getBlogEntry(String token, long entryId)
-   getPage(String token, String spaceKey, String pageTitle)
-   getPage(String token, long pageId)
-   getComments(String token, long pageId)
-   getComment(String token, long commentId)

You can use both APIs in a client, creating content in wiki markup using the version 1 API and retrieving it in the storage format using the version 2 API. This may be a useful stepping stone for clients to move towards the version 2 API. You should note that there is no longer any way to retrive content from Confluence as wiki markup.

To aid the migration from the version 1 API to the version 2 API a new method *convertWikiToStorageFormat(String token, String markup)* has been added to both versions of the API.

## Script Examples

The Confluence User Community space contains various examples of <a href="http://confluence.atlassian.com/display/DISC/scripts" class="external-link">scripts</a>.

## Changelog

The following changes apply to the [Remote Confluence Methods] and [Remote Confluence Data Objects]:

#### Confluence 5.0.3

-   Added the two `removePageVersion` methods.

#### Confluence 4.3

-   Added `removePageWatch`, `removeSpacewatch` and `removePageWatchForUser` methods.
-   Added `getSpaceStatus` and `setSpaceStatus` for space archiving.

#### Confluence 4.0

-   Added version 2 of the remote API.
-   Calls to methods that return page content using the version 1 API will now result in an error. Please use the version 2 API instead.
-   Added method for converting wiki markup to storage format: `convertWikiToStorageFormat(String token, String markup)`

#### Confluence 3.5

-   Added notification methods: `watchPage`, `watchSpace`, `watchPageForUser`, `getWatchersForPage`, `getWatchersForSpace`
-   Added blog post retrieval method: `getBlogEntryByDateAndTitle`
-   Added space management methods: `getTrashContents`, `purgeFromTrash`, `emptyTrash`.
-   Added data objects: `ContentSummaries`, `ContentSummary`.

#### Confluence 2.10

-   Added `updatePage`.

#### Confluence 2.9

-   Search: Removed option 'all' in table of content types and changed the default to 'All'. If you need to search for all types, simply omit the 'type' restriction.
-   Search: Added option 'contributor' to the table of filter options.

#### Confluence 2.3

-   Added `getClusterInformation` and `getClusterNodeStatuses`.

#### Confluence 2.2

-   Added `addPersonalSpace`, `convertToPersonalSpace` and `addProfilePicture`.

#### Confluence 2.1.4

-   Added `getPermissionsForUser`.

#### Confluence 2.0

-   Updated `getLocks()` to `getPagePermissions()`
-   Added `addAttachment`, `getAttachment`, `getAttachmentData`, `removeAttachment` and `moveAttachment` methods to allow remote attachment handling. Note that adding large attachments with this API uses a lot of memory during the `addAttachment` operation.
-   Added `addAnonymousPermissionToSpace`, `addAnonymousPermissionsToSpace` and `removeAnonymousPermissionFromSpace`.
-   Added the addComment and removeComment methods for comment manipulation.
-   Added hasGroup and hasUser methods to determine if a group or user exists.
-   Added editUser method.
-   Added ability to deactivate and reactivate users.
-   Added getActiveUsers method to retrieve a user list.
-   Added ability to change the user password.
-   Added ability to retrieve and modify user information.
-   Added ability to retrieve, add and remove labels.
-   Added getBlogEntryByDayAndTitle

#### Confluence 1.4

-   Added new `exportSpace` and `exportSite` methods to build exports of an individual space or an entire Confluence instance and return with a URL leading to the download.
-   Added new `getChildren` and `getDescendents` methods to get the direct children and all descendents of a given page.
-   Added new `getAncestors` method to get the ancestors of a given page.
-   Removed the old `getLocks` as locks are superceded by page level permissions.
-   Added new `getPagePermissions` method to retrieve page level permissions.
-   Added new `removeUser`, `removeGroup`, `removeAllPermissionsForGroup`, `addUserToGroup` and `removeUserFromGroup` methods.
-   Added new `addPermissionToSpace` method.
-   Added new `Permission` data object.
-   Added new `getSpaceLevelPermissions` method.

#### Confluence 1.3

-   Added new `getPage` method which retrieves a page by space key and page title.
-   Added new `removeSpace` method to remove an entire space.
-   Added ability to limit search by parameters.
-   Allow anonymous access.

#### Confluence 1.2

-   `renderContent` takes an optional hashtable for rendering hints, the only one supported right now is "style=clean"

#### Confluence 1.1

-   `getLocks` gives you back a list of any locks that apply to a given page
-   added a `locks` field to the various Page structs containing a count of any applicable page-level locks
-   CRUD methods added for blog-posts

#### Confluence 1.0.3

-   `getServerInfo` gives you some basic information about the server version <a href="https://jira.atlassian.com/browse/CONF-1123" class="external-link">CONF1123@JIRA</a>
-   `storePage` now allows you to change the page's name (incoming links are all renamed) <a href="https://jira.atlassian.com/browse/CONF-974" class="external-link">CONF-974@JIRA</a>
-   `storePage` now reliably allows you to re-parent pages
-   WSDL now respects the server's configured base URL, allowing it to work on proxy-hosted servers <a href="https://jira.atlassian.com/browse/CONF-1088" class="external-link">CONF-1088@JIRA</a>

##### RELATED TOPICS

-   [Remote Confluence Data Objects]
-   [Remote Confluence Methods]

  [REST API]: /confcloud/confluence-rest-api-39985291.html
  [SOAP details below]: #soap-details-below
  [Creating an XML-RPC Client]: #creating-an-xml-rpc-client
  [Creating a SOAP Client]: #creating-a-soap-client
  [Remote API version 1 and version 2]: #remote-api-version-1-and-version-2
  [Script Examples]: #script-examples
  [Changelog]: #changelog
  [Remote Confluence Data Objects]: /confcloud/remote-confluence-data-objects-39985848.html
  [Remote Confluence Methods]: /confcloud/remote-confluence-methods-39985850.html
  [see below]: #see-below

