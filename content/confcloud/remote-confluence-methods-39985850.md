---
title: Remote Confluence Methods 39985850
aliases:
    - /confcloud/remote-confluence-methods-39985850.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39985850
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39985850
confluence_id: 39985850
platform:
product:
category:
subcategory:
---
# Confluence Connect : Remote Confluence Methods

The XML-RPC and SOAP APIs are **deprecated since** **Confluence 5.5**.
Confluence has a new [REST API] that is progressively replacing our existing APIs. We recommend plugin developers use the new REST APIs where possible.

This page documents the methods available on the [remote Confluence API].  You can view <a href="http://docs.atlassian.com/atlassian-confluence/latest/index.html?com/atlassian/confluence/rpc/soap/ConfluenceSoapService.html" class="external-link">the Javadoc for this API</a> instead.  The following categories of methods are available:

-   [Authentication Methods]
-   [Administration]
-   [General]
-   [Spaces]
-   [Pages]
-   [Attachments]
-   [Blog Entries]
-   [Notifications]
-   [Search]
-   [Permissions]
-   [User Management]
-   [Labels]

 
Many of the remote methods return a remote data object.  It is a good idea to [review the available remote data objects] before using these methods.

## Authentication Methods

The Javadoc for the authentication methods are <a href="http://docs.atlassian.com/atlassian-confluence/latest/com/atlassian/confluence/rpc/SecureRpc.html" class="external-link">also available</a>.

-   String login(String username, String password) - log in a user. Returns a String authentication token to be passed as authentication to all other remote calls. Must be called before any other method in a *remote conversation*. From 1.3 onwards, you can supply an empty string as the token to be treated as being the anonymous user.
-   boolean logout(String token) - remove this token from the list of logged in tokens. Returns true if the user was logged out, false if they were not logged in in the first place.

## Administration

-   String exportSite (String token, boolean exportAttachments) - exports a Confluence instance and returns a String holding the URL for the download. The boolean argument indicates whether or not attachments ought to be included in the export. This method respects the property `admin. ui .allow.manual.backup.download` (as described on <a href="https://confluence.atlassian.com/display/DOC/Manually+Backing+Up+the+Site#ManuallyBackingUptheSite-Enablingthedownloadofthebackupfileviatheadministrationconsole" class="external-link">this page in confluence documentation</a>); if the property is not set, or is set to false, this method will not return the download link, but instead return a string containing the actual path on the server where the export is located.
-   ClusterInformation getClusterInformation (String token) - returns information about the cluster this node is part of.
-   Vector getClusterNodeStatuses (String token) - returns a Vector of NodeStatus objects containing information about each node in the cluster.
-   boolean isPluginEnabled (String token, String pluginKey) - returns true if the plugin is installed and enabled, otherwise false.
-   boolean installPlugin (String token, String pluginFileName, byte\[\] pluginData) - installs a plugin in Confluence. Returns false if the file is not a JAR or XML file. Throws an exception if the installation fails for another reason.

## General

-   ServerInfo  getServerInfo (String token) - retrieve some basic information about the server being connected to. Useful for clients that need to turn certain features on or off depending on the version of the server. (Since 1.0.3)
-   String convertWikiToStorageFormat (String token, String markup) - converts wiki markup to the storage format and returns it. (Since 4.0)

## Spaces

### **Retrieval**

-   Vector&lt; SpaceSummary &gt; getSpaces (String token) - returns all the summaries that the current user can see.
-   Space  getSpace (String token, String spaceKey) - returns a single Space. If the spaceKey does not exist: earlier versions of Confluence will throw an Exception. Later versions (3.0+) will return a null object.
-   String exportSpace (String token, String spaceKey, String exportType) - exports a space and returns a String holding the URL for the download. The export type argument indicates whether or not to export in XML or HTML format - use "TYPE\_XML" or "TYPE\_HTML" respectively. (**Note:** In Confluence 3.0, the remote API specification for PDF exports changed. You can no longer use this 'exportSpace' method to export a space to PDF. Please refer to [Remote API Specification for PDF Export] for current remote API details on this feature.)
-   String exportSpace (String token, String spaceKey, String exportType, boolean exportAll) - exports a space and returns a String holding the URL for the download. In the [version 2 API] you can set exportAll to true to export the entire contents of the space.
-   String getSpaceStatus (String token, String spaceKey) - returns the status of a space, either CURRENT or ARCHIVED.

### **Management**

-   Space   addSpace (String token, Space  space) - create a new space, passing in name, key and description.
-   Boolean removeSpace (String token, String spaceKey) - remove a space completely.
-   Space   addPersonalSpace (String token, Space  personalSpace, String userName) - add a new space as a personal space.
-   boolean convertToPersonalSpace (String token, String userName, String spaceKey, String newSpaceName, boolean updateLinks) - convert an existing space to a personal space.
-   Space   storeSpace (String token, Space space) - create a new space if passing in a name, key and description **or** update the properties of an existing space. Only name, homepage or space group can be changed.
-   boolean importSpace (String token, byte\[\] zippedImportData) - import a space into Confluence. **Note that this uses a lot of memory - about 4 times the size of the upload.** The data provided should be a zipped XML backup, the same as exported by Confluence.
-   ContentSummaries  getTrashContents (String token, String spaceKey, int offset, int count) - get the contents of the trash for the given space, starting at 'offset' and returning at most 'count' items.
-   boolean purgeFromTrash (String token, String spaceKey, long contentId) - remove some content from the trash in the given space, deleting it permanently.
-   boolean emptyTrash (String token, String spaceKey) - remove all content from the trash in the given space, deleting them permanently.
-   Boolean setSpaceStatus (String token, String spaceKey, String status) - set a new status for a space. Valid values for status are "CURRENT" and "ARCHIVED".

## Pages

### Retrieval

-   Vector&lt; PageSummary &gt; getPages (String token, String spaceKey) - returns all the summaries in the space. Doesn't include pages which are in the Trash. Equivalent to calling `Space. getCurrentPages ()`.

<!-- -->

-   Page  getPage (String token, Long pageId) - returns a single Page
-   Page getPage (String token, String spaceKey, String pageTitle) - returns a single Page
-   Vector&lt; PageHistorySummary &gt; getPageHistory (String token, String pageId) - returns all the PageHistorySummaries - useful for looking up the previous versions of a page, and who changed them.

### Permissions

-   Vector&lt; ContentPermissionSet &gt; getContentPermissionSets (String token, String contentId) - returns all the page level permissions for this page as ContentPermissionSets
-   Hashtable getContentPermissionSet (String token, String contentId, String permissionType) - returns the set of permissions on a page as a map of type to a list of ContentPermission, for the type of permission which is either 'View' or 'Edit'
-   boolean setContentPermissions (String token, String contentId, String permissionType, Vector permissions) - sets the page-level permissions for a particular permission type (either 'View' or 'Edit') to the provided vector of ContentPermissions. If an empty list of permissions are passed, all page permissions for the given type are removed. If the existing list of permissions are passed, this method does nothing.

### Dependencies

-   Vector&lt; Attachment &gt; getAttachments (String token, String pageId) - returns all the Attachments for this page (useful to point users to download them with the full file download URL returned).
-   Vector&lt; PageSummary &gt; getAncestors (String token, String pageId) - returns all the ancestors of this page (parent, parent's parent etc).
-   Vector&lt; PageSummary &gt; getChildren (String token, String pageId) - returns all the direct children of this page.
-   Vector&lt; PageSummary &gt; getDescendents (String token, String pageId) - returns all the descendants of this page (children, children's children etc).
-   Vector&lt; Comment &gt; getComments (String token, String pageId) - returns all the comments for this page.
-   Comment  getComment (String token, String commentId) - returns an individual comment.
-   Comment addComment(String token, Comment comment) - adds a comment to the page.
-   Comment  editComment (String token, Comment comment) - Updates an existing comment on the page.
-   boolean removeComment (String token, String commentId) - removes a comment from the page.

### Management

-   Page  storePage (String token, Page  page) - adds or updates a page. For adding, the Page given as an argument should have space, title and content fields at a minimum. For updating, the Page given should have id, space, title, content and version fields at a minimum. The parentId field is always optional. All other fields will be ignored. The content is in <a href="https://confluence.atlassian.com/display/DOC/Confluence+Storage+Format" class="external-link">storage format</a>. Note: the return value can be null, if an error that did not throw an exception occurred.  Operates exactly like updatePage () if the page already exists.
-   Page   updatePage (String token, Page  page, PageUpdateOptions pageUpdateOptions ) - updates a page. The Page given should have id, space, title, content and version fields at a minimum. The parentId field is always optional. All other fields will be ignored. Note: the return value can be null, if an error that did not throw an exception occurred.
-   String renderContent (String token, String spaceKey, String pageId, String content) - returns the HTML rendered content for this page. The behaviour depends on which arguments are passed:
    -   If only pageId is passed then the current content of the page will be rendered.
    -   If a pageId and content are passed then the content will be rendered as if it were the body of that page.
    -   If a spaceKey and content are passed then the content will be rendered as if it were on a new page in that space.
    -   Whenever a spaceKey and pageId are passed the spaceKey is ignored.
    -   If neither spaceKey nor pageId are passed then an error will be returned.
-   String renderContent (String token, String spaceKey, String pageId, String content, Hashtable parameters) - Like the above renderContent (), but you can supply an optional hash (map, dictionary, etc) containing additional instructions for the renderer. Currently, only one such parameter is supported:
    -   "style = clean" Setting the "style" parameter to "clean" will cause the page to be rendered as just a single block of HTML within a div, without the HTML preamble and stylesheet that would otherwise be added.
-   void removePage (String token, String pageId) - removes a page
-   void removePageVersionById (String token, String historicalPageId)  - removes a historical version of a page identified by that versions id.
-   void removePageVersionByVersion (String token, String pageId, int version) - removes a historical version of a page identified by the current page id and the version number you want to remove (with 1 being the first version)
-   void movePage (String token, String sourcePageId, String targetPageId, String position) - moves a page's position in the hierarchy.
    -   sourcePageId - the id of the page to be moved.
    -   targetPageId - the id of the page that is relative to the sourcePageId page being moved.
    -   position - "above", "below", or "append". (Note that the terms 'above' and 'below' refer to the relative vertical position of the pages in the page tree.)
-   void movePageToTopLevel (String pageId, String targetSpaceKey) - moves a page to the top level of the target space. This corresponds to <a href="http://docs.atlassian.com/atlassian-confluence/3.0/com/atlassian/confluence/pages/PageManager.html#movePageToTopLevel%28com.atlassian.confluence.pages.Page,%20com.atlassian.confluence.spaces.Space%29" class="external-link">PageManager - movePageToTopLevel</a>.

### Position Keys for Moving a Page

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Position Key</p></th>
<th><p>Effect</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>above</p></td>
<td><p>source and target become/remain sibling pages and the source is moved above the target in the page tree.</p></td>
</tr>
<tr class="even">
<td><p>below</p></td>
<td><p>source and target become/remain sibling pages and the source is moved below the target in the page tree.</p></td>
</tr>
<tr class="odd">
<td><p>append</p></td>
<td><p>source becomes a child of the target</p></td>
</tr>
</tbody>
</table>

## Attachments

### Retrieval

-   Attachment  getAttachment (String token, String pageId, String fileName, String versionNumber) - get information about an attachment.
-   byte \[\] getAttachmentData (String token, String pageId, String fileName, String versionNumber) - get the contents of an attachment.

![(info)] To retrieve information or content from the current version of an attachment, use a 'versionNumber' of "0".

### Management

-   Attachment addAttachment(String token, long contentId, Attachment  attachment, byte\[\] attachmentData) - add a new attachment to a content entity object. **Note that this uses a lot of memory - about 4 times the size of the attachment.** The 'long contentId' is actually a String pageId for XML-RPC. Be aware of  <a href="https://jira.atlassian.com/browse/CONF-31169" class="external-link">CONF-31169</a> and <a href="https://jira.atlassian.com/browse/CONF-30024" class="external-link">CONF-30024</a>.
-   boolean removeAttachment (String token, String contentId, String fileName) - remove an attachment from a content entity object.
-   boolean moveAttachment (String token, String originalContentId, String originalName, String newContentEntityId, String newName) - move an attachment to a different content entity object and/or give it a new name.

## Blog Entries

-   Vector&lt; BlogEntrySummary &gt; getBlogEntries (String token, String spaceKey) - returns all the summaries in the space.
-   BlogEntry  getBlogEntry (String token, String pageId) - returns a single entry.
-   BlogEntry  storeBlogEntry (String token, BlogEntry entry) - add or update a blog entry. For adding, the BlogEntry given as an argument should have space, title and content fields at a minimum. For updating, the entry given should have id, space, title, content and version fields at a minimum. All other fields will be ignored.
-   BlogEntry  getBlogEntryByDayAndTitle (String token, String spaceKey, int dayOfMonth, String postTitle) - Retrieves a blog post by specifying the day it was published in the current month, its title and its space key.
-   BlogEntry  getBlogEntryByDateAndTitle (String token, String spaceKey, int year, int month, int dayOfMonth, String postTitle) - retrieve a blog post by specifying the date it was published, its title and its space key.

## Notifications

The notification methods in the remote API are available in Confluence 3.5 and later.

-   boolean watchPage (String token, long pageId) - watch a page or blog post as the current user, returns false if a space, page or blog is already being watched
-   boolean watchSpace (String token, String spaceKey) - watch a space as the current user, returns false if the space is already watched
-   boolean watchPageForUser (String token, long pageId, String username) - add a watch on behalf of another user (space administrators only)
-   boolean removePageWatch (String token, long pageId) - remove a page or blog post watch as the current user, returns false if the space, page or blog isn't being watched
-   boolean removeSpacewatch (String token, String spaceKey) - remove a space watch as the current user, returns false if the space isn't being watched
-   boolean removePageWatchForUser (String token, long pageId, String username) - remove a watch on behalf of another user (space administrators only)
-   boolean isWatchingPage (String token, long pageId, String username) - check whether a user is watching a page (space administrators only, if the username isn't the current user)
-   boolean isWatchingSpace (String token, String spaceKey, String username) - check whether a user is watching a space (space administrators only, if the username isn't the current user)
-   Vector&lt; User &gt; getWatchersForPage (String token, long pageId) - return the watchers for the page (space administrators only)
-   Vector&lt; User &gt; getWatchersForSpace (String token, String spaceKey) - return the watchers for the space (space administrators only).

## Search

-   Vector&lt; SearchResult &gt; search(String token, String query, int maxResults) - return a list of results which match a given search query (including pages and other content types). This is the same as a performing a parameterised search (see below) with an empty parameter map.
-   Vector&lt; SearchResult &gt; search(String token, String query, Map parameters, int maxResults) - (**since 1.3**) like the previous search, but you can optionally limit your search by adding parameters to the parameter map. If you do not include a parameter, the default is used instead.

### Parameters for Limiting Search Results

<table>
<colgroup>
<col width="25%" />
<col width="25%" />
<col width="25%" />
<col width="25%" />
</colgroup>
<thead>
<tr class="header">
<th><p>key</p></th>
<th><p>description</p></th>
<th><p>values</p></th>
<th><p>default</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>spaceKey</p></td>
<td><p>search a single space</p></td>
<td><p>(any valid space key)</p></td>
<td><p>Search all spaces</p></td>
</tr>
<tr class="even">
<td><p>type</p></td>
<td><p>Limit the content types of the items to be returned in the search results.</p></td>
<td><p>page<br />
blogpost<br />
mail<br />
comment<br />
attachment<br />
spacedesc<br />
userinfo</p>
<p>personalspacedesc</p></td>
<td><p>Search all types</p></td>
</tr>
<tr class="odd">
<td><p>modified</p></td>
<td><p>Search recently modified content</p></td>
<td><p>TODAY<br />
YESTERDAY<br />
LASTWEEK<br />
LASTMONTH</p></td>
<td><p>No limit</p></td>
</tr>
<tr class="even">
<td><p>contributor</p></td>
<td><p>The original creator or any editor of Confluence content. For mail, this is the person who imported the mail, not the person who sent the email message.</p></td>
<td><p>Username of a Confluence user.</p></td>
<td><p>Results are not filtered by contributor</p></td>
</tr>
</tbody>
</table>

## Permissions

-   Vector&lt;String&gt; getPermissions (String token, String spaceKey) - Returns a Vector of Strings representing the permissions the current user has for this space (a list of "view", "modify", "comment" and / or "admin").
-   Vector&lt;String&gt; getPermissionsForUser (String token, String spaceKey, String userName) - Returns a Vector of Strings representing the permissions the given user has for this space. (since 2.1.4)
-   Vector&lt; ContentPermission &gt; getPagePermissions(String token, String pageId)  - Returns a Vector of permissions representing the permissions set on the given page.
-   Vector&lt;String&gt; getSpaceLevelPermissions (String token) - returns all of the space level permissions which may be granted. This is a list of possible permissions to use with `addPermissionToSpace`, below, not a list of current permissions on a Space.
-   SpacePermissionSet\[\] getSpacePermissionSets(String token, String spaceKey)  -  retrieves all permission sets specified for the space with given `spaceKey`.
-   SpacePermissionSet getSpacePermissionSet(String token, String spaceKey, String permissionType) - retrieves a specific permission set of type `permissionType` for the space with given `spaceKey`. Valid permission types are listed below, under "Space permissions".
-   boolean addPermissionToSpace (String token, String permission, String remoteEntityName, String spaceKey) - Give the entity named `remoteEntityName` (either a group or a user) the permission `permission` on the space with the key `spaceKey`.
-   boolean addPermissionsToSpace (String token, Vector permissions, String remoteEntityName, String spaceKey) - Give the entity named `remoteEntityName` (either a group or a user) the permissions `permissions` on the space with the key `spaceKey`.
-   boolean removePermissionFromSpace (String token, String permission, String remoteEntityName, String spaceKey) - Remove the permission `permission} from the entity named {{remoteEntityName` (either a group or a user) on the space with the key `spaceKey`.
-   boolean addAnonymousPermissionToSpace (String token, String permission, String spaceKey) - Give anonymous users the permission `permission` on the space with the key `spaceKey`. (since 2.0)

<!-- -->

-   boolean addAnonymousPermissionsToSpace (String token, Vector permissions, String spaceKey) - Give anonymous users the permissions `permissions` on the space with the key `spaceKey`. (since 2.0)
-   boolean removeAnonymousPermissionFromSpace (String token, String permission,String spaceKey) - Remove the permission `permission} from anonymous users on the space with the key {{spaceKey`. (since 2.0)
-   boolean removeAllPermissionsForGroup (String token, String groupname) - Remove all the global and space level permissions for `groupname`.

### Space permissions

Names are as shown in Space Admin &gt; Permissions. Values can be passed to remote API methods above which take a space permission parameter.

<table>
<colgroup>
<col width="33%" />
<col width="33%" />
<col width="33%" />
</colgroup>
<thead>
<tr class="header">
<th><p>Permission name</p></th>
<th><p>String value</p></th>
<th><p>Description</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>View</p></td>
<td><p><code>VIEWSPACE</code></p></td>
<td><p>View all content in the space</p></td>
</tr>
<tr class="even">
<td><p>Pages - Create</p></td>
<td><p><code>EDITSPACE</code></p></td>
<td><p>Create new pages and edit existing ones</p></td>
</tr>
<tr class="odd">
<td><p>Pages - Export</p></td>
<td><p><code>EXPORTPAGE</code></p></td>
<td><p>Export pages to PDF, Word</p></td>
</tr>
<tr class="even">
<td><p>Pages - Restrict</p></td>
<td><p><code>SETPAGEPERMISSIONS</code></p></td>
<td><p>Set page-level permissions</p></td>
</tr>
<tr class="odd">
<td><p>Pages - Remove</p></td>
<td><p><code>REMOVEPAGE</code></p></td>
<td><p>Remove pages</p></td>
</tr>
<tr class="even">
<td><p>News - Create</p></td>
<td><p><code>EDITBLOG</code></p></td>
<td><p>Create news items and edit existing ones</p></td>
</tr>
<tr class="odd">
<td><p>News - Remove</p></td>
<td><p><code>REMOVEBLOG</code></p></td>
<td><p>Remove news</p></td>
</tr>
<tr class="even">
<td><p>Comments - Create</p></td>
<td><p><code>COMMENT</code></p></td>
<td><p>Add comments to pages or news in the space</p></td>
</tr>
<tr class="odd">
<td><p>Comments - Remove</p></td>
<td><p><code>REMOVECOMMENT</code></p></td>
<td><p>Remove the user's own comments</p></td>
</tr>
<tr class="even">
<td><p>Attachments - Create</p></td>
<td><p><code>CREATEATTACHMENT</code></p></td>
<td><p>Add attachments to pages and news</p></td>
</tr>
<tr class="odd">
<td><p>Attachments - Remove</p></td>
<td><p><code>REMOVEATTACHMENT</code></p></td>
<td><p>Remove attachments</p></td>
</tr>
<tr class="even">
<td><p>Mail - Remove</p></td>
<td><p><code>REMOVEMAIL</code></p></td>
<td><p>Remove mail</p></td>
</tr>
<tr class="odd">
<td><p>Space - Export</p></td>
<td><p><code>EXPORTSPACE</code></p></td>
<td><p>Export space to HTML or XML</p></td>
</tr>
<tr class="even">
<td><p>Space - Admin</p></td>
<td><p><code>SETSPACEPERMISSIONS</code></p></td>
<td><p>Administer the space</p></td>
</tr>
</tbody>
</table>

![(info)] In Confluence 3.0, the remote API specification for PDF exports changed. Please refer to [Remote API Specification for PDF Export] for current remote API details on this feature.

## User Management

-   User getUser(String token, String username) - get a single user
-   void addUser(String token, User   user, String password) - add a new user with the given password
-   void addUser(String token, User   user, String password, boolean notifyUser) - add a new user with the given password, and optionally send the user a welcome email (since 4.3)
-   void addGroup(String token, String group) - add a new group
-   Vector&lt;String&gt; getUserGroups (String token, String username) - get a user's current groups
-   void addUserToGroup(String token, String username, String groupname) - add a user to a particular group
-   boolean removeUserFromGroup (String token, String username, String groupname) - remove a user from a group.
-   boolean removeUser(String token, String username) - delete a user.
-   boolean removeGroup (String token, String groupname, String defaultGroupName) - remove a group. If `defaultGroupName` is specified, users belonging to `groupname` will be added to `defaultGroupName`.
-   Vector&lt;String&gt; getGroups (String token) - gets all groups
-   boolean hasUser (String token,  String username) - checks if a user exists
-   boolean hasGroup (String token, String groupname) - checks if a group exists
-   boolean editUser (String token, RemoteUser  remoteUser) - edits the details of a user
-   boolean deactivateUser (String token, String username) - deactivates the specified user
-   boolean reactivateUser (String token, String username) - reactivates the specified user
-   Vector&lt;String&gt; getActiveUsers(String token, boolean viewAll) - returns all registered users
-   boolean setUserInformation(String token, UserInformation userInfo) - updates user information
-   UserInformation getUserInformation(String token, String username) - Retrieves user information
-   boolean changeMyPassword(String token, String oldPass, String newPass) - changes the current user's password
-   boolean changeUserPassword(String token, String username, String newPass) - changes the specified user's password
-   boolean addProfilePicture(String token, String userName, String fileName, String mimeType, byte\[\] pictureData) - add and set the profile picture for a user.

## Labels

-   Vector getLabelsById(String token, long objectId) - Returns all [labels] for the given ContentEntityObject ID
-   Vector getMostPopularLabels(String token, int maxCount) - Returns the most popular labels for the Confluence instance, with a specified maximum number.
-   Vector getMostPopularLabelsInSpace(String token, String spaceKey, int maxCount) - Returns the most popular labels for the given `spaceKey`, with a specified maximum number of results.
-   Vector getRecentlyUsedLabels(String token, int maxResults) - Returns the recently used labels for the Confluence instance, with a specified maximum number of results.
-   Vector getRecentlyUsedLabelsInSpace(String token, String spaceKey, int maxResults) - Returns the recently used labels for the given `spaceKey`, with a specified maximum number of results.
-   Vector getSpacesWithLabel(String token, String labelName) - Returns an array of spaces that have been labelled with `labelName`.
-   Vector getRelatedLabels(String token, String labelName, int maxResults) - Returns the labels related to the given label name, with a specified maximum number of results.
-   Vector getRelatedLabelsInSpace(String token, String labelName, String spaceKey, int maxResults) - Returns the labels related to the given label name for the given `spaceKey`, with a specified maximum number of results.
-   Vector getLabelsByDetail(String token, String labelName, String namespace, String spaceKey, String owner) - Retrieves the labels matching the given `labelName`, `namespace`, `spaceKey` or `owner`.
-   Vector getLabelContentById(String token, long labelId) - Returns the content for a given label ID
-   Vector getLabelContentByName(String token, String labelName) - Returns the content for a given label name.
-   Vector getLabelContentByObject(String token, Label labelObject) - Returns the content for a given Label  object.
-   Vector getSpacesContainingContentWithLabel(String token, String labelName) - Returns all spaces that have content labelled with `labelName`.
-   boolean addLabelByName(String token, String labelName, long objectId) - Adds label(s) to the object with the given ContentEntityObject ID. For multiple labels, labelName should be in the form of a space-separated or comma-separated string.
-   boolean addLabelById(String token, long labelId, long objectId) - Adds a label with the given ID to the object with the given ContentEntityObject ID.
-   boolean addLabelByObject(String token, Label   labelObject, long objectId) - Adds the given label object to the object with the given ContentEntityObject ID.
-   boolean addLabelByNameToSpace(String token, String labelName, String spaceKey) - Adds a label to description of a space with the given space key. Prefix `labelName` with "`team:`" in order to make it a space category.
-   boolean removeLabelByName(String token, String labelName, long objectId) - Removes the given label from the object with the given ContentEntityObject ID.
-   boolean removeLabelById(String token, long labelId, long objectId) - Removes the label with the given ID from the object with the given ContentEntityObject ID.
-   boolean removeLabelByObject(String token, Label   labelObject, long objectId) - Removes the given label object from the object with the given ContentEntityObject ID.
-   boolean removeLabelByNameFromSpace(String token, String labelName, String spaceKey) - Removes the given label from the given `spaceKey`.

  [REST API]: /confcloud/confluence-rest-api-39985291.html
  [remote Confluence API]: /confcloud/confluence-xml-rpc-and-soap-apis-39985844.html
  [Authentication Methods]: #authentication-methods
  [Administration]: #administration
  [General]: #general
  [Spaces]: #spaces
  [Pages]: #pages
  [Attachments]: #attachments
  [Blog Entries]: #blog-entries
  [Notifications]: #notifications
  [Search]: #search
  [Permissions]: #permissions
  [User Management]: #user-management
  [Labels]: #labels
  [review the available remote data objects]: /confcloud/remote-confluence-data-objects-39985848.html
  [Remote API Specification for PDF Export]: /confcloud/remote-api-specification-for-pdf-export-39985853.html
  [version 2 API]: https://developer.atlassian.com/display/CONFCLOUD/Confluence+XML-RPC+and+SOAP+APIs#ConfluenceXML-RPCandSOAPAPIs-v2apiRemoteAPIversion1andversion2
  [(info)]: /confcloud/images/icons/emoticons/information.png
  [labels]: https://developer.atlassian.com/display/CONFCLOUD/Remote+Confluence+Data+Objects#RemoteConfluenceDataObjects-Label

