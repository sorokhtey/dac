---
title: Extending the agent view via the JIRA platform 
platform: cloud
product: jsdcloud
category: devguide
subcategory: learning
guides: guides
aliases:
- /jiracloud/guide-extending-the-agent-view-via-the-jira-platform-39988302.html
- /jiracloud/guide-extending-the-agent-view-via-the-jira-platform-39988302.md
confluence_id: 39988302
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39988302
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39988302
date: "2016-08-22"
---
# Extending the agent view via the JIRA platform

JIRA Service Desk is its own product, however it is built on top of the JIRA platform. This means that you can make use of the JIRA platform APIs and modules when extending JIRA Service Desk.

The agent view in JIRA Service Desk is actually a custom version of the standard JIRA issue view (unlike the customer portal, which is entirely unique to Service Desk). As a result, you can extend the agent view in many of the same ways that you extend the basic JIRA user interface.

This guide will show you a few examples of how to extend the agent view by using the functionality of the JIRA platform and Atlassian Connect. 

**On this page:**

-   [Extending the agent view sidebar](#extending-the-agent-view-sidebar)
-   [Adding custom configuration pages](#adding-custom-configuration-pages)
-   [Extending the agent request view](#extending-the-agent-request-view)
-   [Next steps](#next-steps)

## Extending the agent view sidebar

The JIRA sidebar is the primary means by which agents will navigate through the agent view. With Atlassian Connect, you can extend the sidebar to include your own navigation items, allowing add-ons to provide a first-class experience for agent users.

To add an item to the sidebar, simply define a` jiraProjectTabPanel`module within your `atlassian-connect.json` descriptor as shown below. The given `name` will serve as both the text of the link within the sidebar, as well as the title of the page content.

``` js
...
"modules": {
    "jiraProjectTabPanels": [
        {
            "key": "jira-project-sidebar-link",
            "name": {
                "value": "My JIRA project sidebar item"
            },
            "url": "/jira-project-sidebar-content"
        }
    ]
}
...
```

![JIRA Service Desk page showing new sidebar item](../images/jdev-project-sidebar-link.png)

## Adding custom configuration pages

Within the agent view, the project settings area is where project administrators make use of JIRA Service Desk's extensive customisability in order to create the best solutions for their customers. Through Atlassian Connect, you can add your own configuration pages to the agent view, allowing add-ons to seamlessly integrate their configuration pages into the JIRA Service Desk UI.

To add a configuration page, simply define a `jiraProjectAdminTabPanel` module within your `atlassian-connect.json` descriptor as shown below. 

The valid locations include: projectgroup1, projectgroup2, projectgroup3, projectgroup4

``` js
...
"modules": {
    "jiraProjectAdminTabPanels": [
        {
            "key": "jira-project-settings-sidebar-link",
            "location": "projectgroup1",
            "name": {
                "value": "My JIRA project settings sidebar item"
            },
            "url": "/jira-project-settings-sidebar-content"
        }
    ]
}
...
```

![JIRA Service Desk page showing custom configuration page](../images/jdev-project-settings-sidebar-link.png)

## Extending the agent request view

Of all the areas in the agent view, the agent request view is perhaps the most central and common, serving as an operational hub for agents as they work with requests. As the agent request view is simply a JIRA issue view in disguise, you can leverage existing Atlassian Connect functionality to add to and extend various areas within the page. This allows add-ons to display additional request information, or even provide entirely new functionality to agent users.

As an example, to add a panel to the right-hand side of the agent request view, simply define a` webPanel` module within your `atlassian-connect.json` descriptor as shown below.

``` js
...
"modules": {
    "webPanels": [
        {
            "key": "jira-issue-panel-content",
            "url": "/jira-issue-panel-content",
            "location": "atl.jira.view.issue.right.context",
            "name": {
                "value": "My JIRA issue panel"
            }
        }
    ]
}
...
```

![JIRA Service Desk page showing new panel on agent request view](../images/jdev-issue-panel-content.png)

{{% tip title="Congratulations!"%}}You now know how to extend the agent view.{{% /tip %}} 

## Next steps

{{% note title="Stuck?"%}}Don't worry, we all get stuck sometimes. Check out this page: [Get help](/cloud/jira/service-desk/get-help).{{% /note %}} 

If you've finished this tutorial, check out these other resources:

-   [JIRA Service Desk tutorials] -- Try your hand at another tutorial.

  [JIRA Service Desk tutorials]: /cloud/jira/service-desk/tutorials-and-guides