---
title: Automation action
platform: cloud
product: jsdcloud
category: reference
subcategory: modules
aliases:
    - /jiracloud/jira-service-desk-automation-action-module-41222507.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=41222507
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=41222507
confluence_id: 41222507
date: "2016-08-17"
---
# Automation action

This pages lists the JIRA Service Desk modules for the Automation Framework.

{{% note title="This feature is in beta" %}}The automation action module is currently in beta and **disabled by default**. If you want it enabled, add a comment with the URL of your Cloud instance to this ticket: <a href="https://ecosystem.atlassian.net/browse/JSDECO-2" class="external-link">JSDECO-2</a>*.*<br>We'd also love to hear your feedback on this feature. Share your thoughts with us by adding comments to the same ticket above.{{% /note %}}

## Automation action module

The automation action module enables add-ons to extend the Automation Framework in JIRA Service Desk by providing their own "THEN" actions. See [Guide - Implementing automation actions].

#### Module type
`automationActions`

#### Sample JSON
``` json
"modules": {
   "automationActions"                       
        { 
        "key": "my-automation-action", 
         "name": { 
               "value": "My automation action" 
              },
        "configForm": {
                "url": "/configForm"
           },
           "webhook": {
                "url": "/webhook"
            }
        }
    ]
}
```

#### Properties

`key`

-   **Type**: `string (^[a-zA-Z0-9-]+$)`
-   **Required**: yes
-   **Description**: A key to identify this module. This key must be unique relative to the add on, with the exception of Confluence macros: their keys need to be globally unique. Keys must only contain alphanumeric characters and dashes.

`name`

-   **Type**: [i18n Property]
-   **Required**: yes
-   **Description**: A human readable name. This name will be the name visible to the user in the "Add action" screen.

`configForm`

-   **Type**: [URL]
-   **Required**: yes
-   **Description**: The URL of the add-on resource that provides the content of the configuration form for this action. This URL must be relative to the add-on's baseUrl. See [Configuration form] for more information.

`webhook`

-   **Type**: [URL]
-   **Required**: yes
-   **Description**: The URL of the add-on resource that will receive the webhook POST request once the action is triggered. This URL must be relative to the add-on's baseUrl. See [Webhook] for more information.

## Configuration form

The add-on will need to provide a configuration form (an HTML page) which defines the appearance and logic of the action. This form handles things like UI layout, client-side validation, and error message rendering.

The form should be similar to a [Web Panel], but in addition, the form needs to use [Events] and the [Atlassian Connect JavaScript API] to communicate with JIRA Service Desk.

### Events

The add-on will need to listen to the following events that JIRA Service Desk will trigger and respond by calling the corresponding API method with the response.

-   `jira-servicedesk-automation-action-serialize`
    -   Triggered when configuration is valid and is about to be saved
    -   `serialize(object)` must be called with the result of this event

-   `jira-servicedesk-automation-action-validate`
    -   Triggered when the user tries to save the form
    -   `validate(isValid)` must be called with the result of this event

### JavaScript API

JIRA Service Desk provides an Automation JavaScript API module that the add-on will need to use in order to retrieve, store and validate its configuration.

#### Methods
 

``` js
/**
 * Retrieves the configuration object for the action.
 * Note that the configuration will be undefined for new actions.
 *
 * @param {Function} callback - the callback that handles the response.
 */
 getConfig(callback)
 
/**
 * Provides JIRA Service Desk with the configuration object of the form to be serialized and stored. 
 * 
 * Must only be called in response to the jira-servicedesk-automation-action-serialize event
 * @param {Object} object - A map containing the configuration of the form. The map must be a simple (non-nested) object.
 */
 serialize(object)
 
/**
 * Provides JIRA Service Desk with the result of the validation of the configuration.
 * If called with "false", the user will not be able to save the configuration form. Ideally, this would be the time the 
 * add-on would render an error message notifying the user that the configuration is invalid.
 *
 * Must only be called in response to the jira-servicedesk-automation-action-validate event
 * @param {Boolean} isValid - Whether the configuration is valid and can be saved
 *       
 */
 validate(isValid)
```

### Example

This is an example script that for a simple form with just one configuration field "`#my-form-field`".

``` js
$(function () {
    AP.require(["events"], function(events) { 
        var automation = new AP.jiraServiceDesk.Automation();


        automation.getConfig(function(config) {
            if (config) { // config is undefined if this is a new action
                $("#my-form-field").val(config.myFormField);
            }
        });

        events.on("jira-servicedesk-automation-action-serialize", function() {
            automation.serialize({
                myFormField: $("#my-form-field").val()
            });
        });

        events.on("jira-servicedesk-automation-action-validate", function() {
            var isValid = true;
            
            if (!$("#my-form-field").val()) {
                renderErrorMessage("my form field cannot be empty");
                isValid = false;
            }

            automation.validate(isValid);
        });
    });
});
```

 

## Webhook

When the action is triggered, the webhook resource specified by the add-on will recieve a POST request. The JSON payload of the request will contain the following fields:

-   `timestamp`
    -   Time the webhook payload was created in milliseconds.
-   `issue`
    -   The JIRA representation of the issue. This is the same shape returned by the [JIRA REST API].
-   `action`
    -   Contains details about the action that was triggered. This includes the action configuration that was saved previously.

### Example

Continuing the example above, the payload might look like this:

``` js
{
    "timestamp": 1461049397396,
    "issue": {
        "id": "12345",
        "key": "JRA-678"
        "fields": { ... }
    },
    "action": {
        "configuration": {
            "myFormField": "Lorem Ipsum"
        }
    }
}
```

  [Guide - Implementing automation actions]: /cloud/jira/service-desk/guide-implementing-automation-actions
  [URL]: /cloud/jira/platform/connect/modules/url
  [Configuration form]: #configuration-form
  [Webhook]: #webhook
  [Web Panel]: /cloud/jira/service-desk/connect/modules/webPanels
  [Events]: /cloud/jira/service-desk/connect/jsapi/Events
  [JIRA REST API]: https://docs.atlassian.com/jira/REST/cloud/#api/2/issue-getIssue
  [Atlassian Connect JavaScript API]: https://developer.atlassian.com/static/connect/docs/latest/concepts/javascript-api.html

