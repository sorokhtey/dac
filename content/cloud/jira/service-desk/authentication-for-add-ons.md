---
title: "Authentication for add-ons"
platform: cloud
product: jsdcloud
category: devguide
subcategory: security
date: "2016-10-31"
---
{{< include path="content/cloud/connect/concepts/authentication.snippet.md" >}}