---
title: Context parameters
platform: cloud
product: jswcloud
category: devguide
subcategory: blocks
date: "2016-11-02"
---
{{< include path="content/cloud/connect/concepts/context-parameters.snippet.md">}}
