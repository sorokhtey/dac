---
title: Extension points for the View issue page
platform: cloud
product: jswcloud
category: reference
subcategory: modules
date: "2016-11-02"
---

{{< reuse-page path="content/cloud/jira/platform/extension-points-for-the-view-issue-page.md">}}