---
title: Adding a board configuration page
platform: cloud
product: jswcloud
category: devguide
subcategory: learning 
guides: tutorials
aliases:
- /jiracloud/tutorial-adding-a-board-configuration-page-39990300.html
- /jiracloud/tutorial-adding-a-board-configuration-page-39990300.md
confluence_id: 39990300
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39990300
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39990300
date: "2016-05-20"
---
# Adding a board configuration page

This tutorial shows you how to build a board configuration page in JIRA Software, by using a JIRA Software module in your Connect add-on. We won't go into the details of adding settings to the page, but this tutorial will provide a good starting point if you want your add-on to have separate configurations per board.

To create this board configuration page, you'll be creating a new web panel located in `jira.agile.board.configuration`, then adding code to render the web panel. 

{{% note title="About these instructions"%}}You can use any supported combination of OS and IDE to create this plugin. These instructions were written using Sublime Text editor on Mac OS X. If you are using a different IDE and/or OS, you should use the equivalent operations for your specific environment.<br>This tutorial was last tested with **JIRA Software 7.1.0-OD-05**.{{% /note %}}

 **On this page:**

-   [Before you begin](#before-you-begin)
-   [Instructions](#instructions)
-   [Tips](#tips)
-   [Next steps](#next-steps)

## Before you begin

-   If you haven't built a Connect add-on before, read the [Getting started] tutorial. You'll learn the fundamentals of Connect development and build a basic Connect add-on that you can use with this tutorial.

## Instructions

#### Step 1. Add a web panel with board context parameters

To create a board configuration page, you'll need to create a web panel with the label and location set to `jira.agile.board.tools`. 

1.  Edit the **atlassian-connect.json** file in your add-on project.
2.  Add the following code to the `modules` context:

    ``` javascript
    "webPanels": [
          {
            "key": "my-configuration-page",
            "url": "configuration?id={board.id}&type={board.type}",
            "name": {
              "value": "My Configuration Page"
            },
            "location": "jira.agile.board.configuration",
            "weight": 1
          }
        ]
    ```
3.  Save your changes.

#### Step 2. Add code to render the web panel

The server-side code required to render the web panel is shown below. This code is for <a href="https://bitbucket.org/atlassian/atlassian-connect-express" class="external-link">atlassian-connect-express</a>, the node.js framework (with Express), but you can use a different technology stack if you wish.

1.  Navigate to the **routes** directory in your add-on project and edit the **index.js** file.
2.  Add the following code to the file and save it:

    ``` js
    app.get('/configuration', function(req,res){
        res.render("configuration", {id : req.query['id'], type : req.query['type'] });
    });
    ```
    This adds the 'configuration' route to your app.
3.  Navigate to the **views** directory in your add-on project and create a new **configuration.hbs** file. 
4.  Add the following code to the **configuration.hbs** file and save it:
    ``` xml
    <!DOCTYPE html>
    <html>
      <head>
        <link rel="stylesheet" href="//aui-cdn.atlassian.com/aui-adg/5.9.17/css/aui.min.css" media="all">
        <script src="https://ac-getting-started.atlassian.net/atlassian-connect/all.js"></script>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
        <script src="//aui-cdn.atlassian.com/aui-adg/5.9.17/js/aui.min.js"></script>
      </head>
      <body class="aui-page-focused aui-page-focused-xlarge">
        <div id="page">
          <section id="content">
            <header class="aui-page-header">
              <div class="aui-page-header-inner">
                <div class="aui-page-header-main">
                  <h2>Example board configuration page</h2>
                  <p id="custom-message">This is an example configuration page for board ID: {{id}} with board type: {{type}}.</p>
                </div>
              </div>
            </header>
          </section>
        </div>
      </body>
    </html>
    ```

    *Notice that we've used two [context parameters]* *used in the template above: `id` (for board Id) and `type` (for board type).*

#### Step 3. Check that it all works

1.  Start up and deploy your add-on (`npm start`) to your JIRA Cloud instance, if it's not running already. 
2.  Navigate to the board configuration for any JIRA Software project. 
3.  Click the **Example board configuration** link. You should see a page like this:
    ![Example board configuration page](../images/jiradev-boardconfig.png)

{{% tip title="Congratulations!"%}}You've just built a board configuration page for your JIRA Software Connect add-on.{{% /tip %}}

## Tips

-   If you are looking for somewhere to store configuration information, make use of the [board properties].
-   Your add-on can define custom [project] or [global] permissions. This may be useful if you want to limit access to your add-on's board configuration page.

## Next steps

{{% note title="Stuck?"%}}Don't worry, we all get stuck sometimes. Check out this page: [Get help](/cloud/jira/software/get-help).{{% /note %}} 

If you've finished this tutorial, check out these other resources:

-   [JIRA Software tutorials] -- Try your hand at another tutorial.

  [Getting started]: /cloud/jira/software/getting-started
  [context parameters]: /cloud/jira/software/jira-software-context-parameters
  [board properties]: https://docs.atlassian.com/jira-software/REST/cloud/#agile/1.0/board/{boardId}/properties
  [project]: /cloud/jira/software/connect/modules/jiraProjectPermissions
  [global]: /cloud/jira/platform/connect/modules/jiraGlobalPermissions
  [JIRA Software tutorials]: /cloud/jira/software/tutorials-and-guides
