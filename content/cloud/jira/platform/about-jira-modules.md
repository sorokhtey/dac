---
title: About JIRA modules 
platform: cloud
product: jiracloud
category: reference
subcategory: modules
aliases:
- /jiracloud/jira-platform-modules-39987040.html
- /jiracloud/jira-platform-modules-39987040.md
confluence_id: 39987040
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39987040
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39987040
date: "2016-08-08"
---
# About JIRA modules

JIRA modules allow add-ons to extend the functionality of the JIRA platform or a JIRA application. JIRA modules are commonly used to extend the user interface by adding links, panels, etc. However, some JIRA modules can also be used to extend other parts of JIRA, like permissions and workflows. JIRA Service Desk and JIRA Software also have their own application-specific modules (UI-related only).

## Using JIRA modules

You can use a JIRA module by declaring it in your add-on descriptor (under `modules`), with the appropriate properties. For example, the following code adds the `generalPages` module at the `system.top.navigation.bar` location to your add-on, which adds a link in the JIRA header.

**atlassian-connect.json**

``` java
...
"modules": {
          "generalPages": [
              {
                  "key": "activity",
                  "location": "system.top.navigation.bar",
                  "name": {
                      "value": "Activity"
                  }
              }
          ]
      }
...
```

## JIRA platform modules

There are two types of modules: basic iframes that allow you to display content in different locations in JIRA, and more advanced modules that let you provide advanced JIRA-specific functionality.

### Basic modules 

-   [Dialog]: shows content inside of a modal dialog
-   [Page]: displays content on a full screen page within JIRA
-   [Web panel]: displays content in a panel (like on the View Issue screen)
-   [Web item]: adds a link or button to a defined location in JIRA (usually used on conjunction with dialogs or pages)
-   [Web section]: defines a new section to add multiple web items
-   [Webhook]: see the [Webhooks] page for more information

### Basic module locations

-   [Administration console]
-   [End-user locations]
-   [Project configuration]
-   [Project sidebar]
-   [View issue page]

### Advanced modules for JIRA

These advanced modules provide access to pre-defined locations or features in JIRA. 

-   [Dashboard item]: provides a new gadget to display on JIRA dashboards
-   [Entity property]: see the [JIRA Entity properties][Entity property] page for more information
-   [Global permission]: defines a new global permission in JIRA
-   [Project admin tab pane]l: adds a new page to JIRA project settings
-   [Project permission]: defines a new project-level permission in JIRA
-   [Report]: adds a new type of report to the JIRA reports page
-   [Search request view]: renders a custom view of a search result that's accessible from the JIRA issue navigator
-   [Tab panel]: adds panels to the JIRA project sidebar, user profile page, or view issue page
-   [Workflow post-function]: adds a new post-function to a JIRA workflow

  [JIRA Software modules]: /cloud/jira/software/jira-software-modules
  [JIRA Service Desk modules]: /cloud/jira/service-desk/jira-service-desk-modules
  [Dialog]: /cloud/jira/platform/connect/modules/dialogs
  [Page]: /cloud/jira/platform/connect/modules/generalPages
  [Web panel]: /cloud/jira/platform/connect/modules/webPanels
  [Web item]: /cloud/jira/platform/connect/modules/webItems
  [Web section]: /cloud/jira/platform/connect/modules/webSections
  [Webhook]: /cloud/jira/platform/connect/modules/webhooks
  [Webhooks]: /cloud/jira/platform/webhooks
  [Administration console]: /cloud/jira/platform/extension-points-for-the-admin-console
  [End-user locations]: /cloud/jira/platform/extension-points-for-the-end-user-ui
  [Project configuration]: /cloud/jira/platform/extension-points-for-project-configuration
  [Project sidebar]: https://developer.atlassian.com/jiradev/jira-platform/guides/projects/design-guide-jira-project-centric-view/development-guide-jira-project-centric-view
  [View issue page]: /cloud/jira/platform/extension-points-for-the-view-issue-page  
  [Dashboard item]: /cloud/jira/platform/connect/modules/jiraDashboardItems
  [Entity property]: /cloud/jira/platform/connect/modules/jiraEntityProperties
  [Global permission]: /cloud/jira/platform/connect/modules/jiraGlobalPermissions
  [Project admin tab pane]: /cloud/jira/platform/connect/modules/jiraProjectAdminTabPanels
  [Project permission]: /cloud/jira/platform/connect/modules/jiraProjectPermissions
  [Report]: /cloud/jira/platform/connect/modules/jiraReports
  [Search request view]: /cloud/jira/platform/connect/modules/jiraSearchRequestViews
  [Tab panel]: /cloud/jira/platform/connect/modules/jiraIssueTabPanels
  [Workflow post-function]: /cloud/jira/platform/connect/modules/jiraWorkflowPostFunctions
