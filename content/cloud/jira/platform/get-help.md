---
title: Get help 
platform: cloud
product: jiracloud
category: help
subcategory: help
aliases:
- /jiracloud/support-39988134.html
- /jiracloud/support-39988134.md
confluence_id: 39988134
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39988134
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39988134
date: "2016-10-10"
layout: get-help
---

If you are looking for status updates with our products or Atlassian Connect framework, no need to file a ticket. Just check out the latest notifications and status in the links below.
