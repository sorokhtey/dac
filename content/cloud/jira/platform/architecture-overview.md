---
aliases:
    - /jiracloud/architecture-overview.html
    - /jiracloud/architecture-overview.md
title: Architecture overview
platform: cloud
product: jiracloud
category: devguide
subcategory: intro
date: "2016-09-27"
---
{{< include path="content/cloud/connect/concepts/cloud-development.snippet.md">}}
