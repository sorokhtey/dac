# Development setup

<div class="aui-message">
Our development environments are for development and testing use only. They are not intended for production 
use, and are not supported as such.
</div>

This page walks you through setting up an add-on development environment for JIRA and Confluence Cloud.

The steps below guide you through setting up and installing an add-on on your new development environment:

- [Step 1. Getting a development version of JIRA and Confluence](#cloud-dev)
- [Step 2. Enable development mode](#enable-development-mode)
- [Step 3. Install an add-on](#install-addon)

Estimated time: 5 minutes

<a name='cloud-dev'></a>
## Step 1. Getting a development version of JIRA and Confluence

To get your development environment:

1. Go to <a href="http://go.atlassian.com/cloud-dev" target="_blank">go.atlassian.com/cloud-dev</a> and sign up for a free development environment.
1. Follow any instructions and complete the onboarding experience.
   
<a name='enable-development-mode'></a>
## Step 2. Enable development mode

This step will turn development mode on in your JIRA or Confluence instance. This allows you to install add-on 
descriptors from any public URL, rather than requiring a private Marketplace listing and URL. 

To enable development mode on your development environment: 

1. <a name="manage-addons"></a>Go to the *Manage add-ons* page in JIRA or Confluence.
    - For JIRA: Click the cog in the top right of the header and select **Add-ons** from the dropdown. After the page has loaded then select **Manage add-ons** from the left sidebar.
    - For Confluence: Press the '/' key and then type **Manage add-ons** in the quick-nav and select the correct option from the dropdown.
1. On the *Manage add-ons* page, scroll down and click __Settings__.
1. Select __Enable development mode__, and then click **Apply**.
 
After the page refreshes, the <svg xmlns="http://www.w3.org/2000/svg" style="transform: translateY(4px)" width="16px" height="16px" viewBox="0 0 16 16" version="1.1"><g id="Cross-product-icons" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="16/i0223_upload" fill="#000"><path d="M3 14L3 13 13 13 13 14 3 14ZM13.001 6.84L8.201 2 7.811 2 3 6.84 4.12 7.971 7 5.08 7 9.5C7.09 10.5 7.45 11 8 11 8.55 11 8.891 10.63 9 9.5L9 5.09 11.91 7.94 13.001 6.84Z" id="i0223_upload-16"></path></g></g></svg>
__Upload add-on__ button will be visible. 
This button creates a dialog in which to paste the URL of a add-on descriptor to be able to install it.
When you wish to upload an Atlassian Connect add-on to this instance, just enter a link to the add-ons descriptor in the <svg xmlns="http://www.w3.org/2000/svg" style="transform: translateY(4px)" width="16px" height="16px" viewBox="0 0 16 16" version="1.1"><g id="Cross-product-icons" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="16/i0223_upload" fill="#000"><path d="M3 14L3 13 13 13 13 14 3 14ZM13.001 6.84L8.201 2 7.811 2 3 6.84 4.12 7.971 7 5.08 7 9.5C7.09 10.5 7.45 11 8 11 8.55 11 8.891 10.63 9 9.5L9 5.09 11.91 7.94 13.001 6.84Z" id="i0223_upload-16"></path></g></g></svg>
__Upload add-on__ dialog.

<a name='install-addon'></a>
## Step 3. Install an add-on

In this step you are going to install a Hello World add-on into your new cloud development environment.  

1. Go to the **Manage add-ons** page (see above).  
1. Click on the <svg xmlns="http://www.w3.org/2000/svg" style="transform: translateY(4px)" width="16px" height="16px" viewBox="0 0 16 16" version="1.1"><g id="Cross-product-icons" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="16/i0223_upload" fill="#000"><path d="M3 14L3 13 13 13 13 14 3 14ZM13.001 6.84L8.201 2 7.811 2 3 6.84 4.12 7.971 7 5.08 7 9.5C7.09 10.5 7.45 11 8 11 8.55 11 8.891 10.63 9 9.5L9 5.09 11.91 7.94 13.001 6.84Z" id="i0223_upload-16"></path></g></g></svg> __Upload add-on__ button.
1. Copy one of the URLs below, paste it into the text box, and then click __Upload__.   
    - For JIRA, use https://ac-static-tutorial.aerobatic.io/atlassian-connect.json.
    - For Confluence, use https://ac-static-tutorial-confluence.aerobatic.io/atlassian-connect.json.
    
   This downloads the Hello World add-on's descriptor and installs it into your development instance.

To test that the add-on is installed, refresh the page and click the **Click me** link in the top header.

<h1>Congratulations!</h1>
    
You have now installed your first development add-on in your development environment. 

From here you can:

 - Try [the Connect basics tutorial](../tutorials/connect-basics.html),
 - View [the source code][2] of the Hello World application to see how it works, or
 - Read through the [Add-on Descriptor](../modules) documentation to understand how to write your own descriptor


 [1]: http://blogs.atlassian.com/2012/08/jira-tip-of-the-month-dot-and-comma-dialog-shortcuts
 [2]: https://bitbucket.org/atlassianlabs/ac-static-tutorial
 [3]: https://ac-static-tutorial.aerobatic.io