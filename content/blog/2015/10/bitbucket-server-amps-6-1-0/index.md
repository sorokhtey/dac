---
title: "Bitbucket Server now available in the Atlassian Plugin SDK"
author: "rwhitbeck"
date: "2015-10-20T16:00:00+07:00"
lede: "The latest version of the Atlassian Plugin SDK now has support 
for the Bitbucket Server."
categories: ["bitbucket-server", "amps"]
---

The latest version of the [Atlassian Plugin SDK](https://developer.atlassian.com/docs/getting-started) 
now has support for [Bitbucket Server](https://developer.atlassian.com/bitbucket/server/docs/latest/index.html).

<p align="center" style="margin: 35px 0 35px 0;">
  <a href="https://developer.atlassian.com/docs/getting-started"
class="aui-button aui-button-primary" target="_blank" style="font-size: 16px; line-height: 24px;">Get the SDK</a>

You can get the latest version by either [downloading the latest installer](https://developer.atlassian.com/docs/getting-started), 
running [`atlas-update`](https://developer.atlassian.com/docs/developer-tools/working-with-the-sdk/command-reference/atlas-update) 
or using your favorite [Package Management software, like Homebrew](https://developer.atlassian.com/docs/getting-started/downloads#Downloads-Otheroptions). 
Once you have the latest Atlassian Plugin SDK you can then use the `atlas` 
commands with `bitbucket`. For example...

You can start a version of Bitbucket Server for development of add-ons with the following command:

```
$ atlas-run-standalone --product bitbucket
```

You can create a Bitbucket Server add-on skeleton by running the following command:

```
$ atlas-create-bitbucket-plugin
```

You can run the add-on and attach it to the server with a debugger attached at port 5005:

```
$ atlas-debug
```

<p align="center" style="margin: 35px 0 35px 0;">
  <a href="https://developer.atlassian.com/docs/getting-started"
class="aui-button aui-button-primary" target="_blank" style="font-size: 16px; line-height: 24px;">Get the SDK</a>

## Need more help?

Have questions or issues with the SDK the best place to ask is over at 
[Atlassian Answers](http://answers.atlassian.com). 

* [Installing the Atlassian Plugin SDK on your platform](https://developer.atlassian.com/docs/getting-started/set-up-the-atlassian-plugin-sdk-and-build-a-project)
* [Create a plugin skeleton by example](https://developer.atlassian.com/docs/getting-started/learn-the-development-platform-by-example/create-a-plugin-skeleton)
* [Help with `atlas-debug`](https://developer.atlassian.com/docs/developer-tools/working-with-the-sdk/command-reference/atlas-debug)
* [Developing add-ons for Bitbucket Server](https://developer.atlassian.com/bitbucket/server/docs/latest/index.html)