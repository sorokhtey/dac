---
title: "Tip of The Week - Use Liquid Prompt to enhance your Bash or zsh"
date: "2015-09-22T16:00:00+07:00"
author: "pvandevoorde"
categories: ["totw","Bash"]
lede: "Liquid Prompt gives you a nicely displayed prompt with useful information when you need it.
 It shows you what you need when you need it. You will notice what changes when it changes,
 saving time and frustration. You can even use it with your favorite shell - Bash or zsh."
---
This week's article is about using Liquid Prompt to enhance your Bash or zsh shell.

Thanks to <a href="https://developer.atlassian.com/blog/authors/npaolucci/">Nicola Paulucci</a> for the idea.

Liquid Prompt gives you a nicely displayed prompt with useful information when you need it.
It shows you what you need when you need it. You will notice what changes when it changes,
 saving time and frustration. You can even use it with your favorite shell - Bash or zsh.

Some of it's features include:
* the name of the current branch if you are in a version control repository (Git, Mercurial, Subversion, Bazaar, or Fossil):
  * in green if everything is up-to-date
  * in red if there are changes
  * in yellow if there are pending commits to push
* the number of added/deleted lines if changes have been made and the number of pending commits
* a yellow + if there are stashed modifications
* a red \* if there are untracked files in the repository

![Liquid Prompt Screenshot](Liquid-Prompt-black.png "Liquid Prompt")

You can find additional features and installation instructions <a href="https://github.com/nojhan/liquidprompt">here</a>.

Have fun using it!
