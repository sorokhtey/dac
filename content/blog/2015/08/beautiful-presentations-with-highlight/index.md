---
title: "Creating beautiful presentations using Highlight"
date: "2015-08-18T16:00:00+07:00"
author: "dtester"
categories: ["tools", "presentations"]
lede: "Creating presentations with nice code snippets can be tough. I'll show 
you how to use the highlight command line tool to make code snippets in 
presentations look great!"
---

When I'm preparing content for a presentation deck, I often find myself 
struggling to get code snippets from Sublime Text to Keynote. The code loses 
highlighting and indentation. After having this frustrating experience, I looked 
for and found a nice little command line tool called [Highlight][highlight] by 
Andre Simon that helps with the struggle.

## Why Highlight?
Highlight is simple, cross-platform, and incredibly flexible. For me, I work on 
the command line a lot so having a tool that I can access immediately is great. 
The nice thing is that highlight has a GUI tool that contains a subset of the 
command line tool and a realtime preview. You can learn how to build the GUI in 
the [installation instructions][install]. 

The [plug-ins][plugins] and themes provide flexibility to support specific file 
types and to tailor your output for a given presentation deck. Highlight ships 
with quite a few themes that you can find in the share directory after installation.

## Get out your highlighters
The first thing you'll need to do is install ```highlight```. My preferred method 
for OS X is to use [brew][homebrew], which makes it super simple:

```bash
brew install highlight
```

For Windows, you can simply [download the Windows installer][windows] and follow 
the instructions. Easy!

## Highlight your first file
My primary workflow is sending a code file into ```highlight``` and piping the 
output to pbcopy to get it into the clipboard. From there, I can easily paste it 
into Keynote and do any necessary resizing to make it readable on screen.
```bash
highlight -O rtf file.js | pbcopy
```

This example is the simplest example of the command line options. Note that the 
```rtf``` output format is required for pasting into Keynote. This preserves the 
formatting and coloring. 

Aside from setting the output, Highlight has a lot of more 
available options, which you can find those in the [manual][cli-options]. I would 
like to...highlight...a few of my favorites:

- ```-k``` or ```--font``` - sets the font face
- ```-K``` or ```--font-size``` - sets the font size
- ```-t``` or ```--tab``` - sets the tab length
- ```-s``` or ```--style``` - sets the theme

My usual command line looks something like this:
```bash
highlight -O rtf -s earendel -k 'Courier' -K 50 file.js | pbcopy
```
That line tells highlight to style the output according to the "earendel" 
theme, use the Courier font, and size the font to 50 points. The output 
looks like this once pasted inside of Keynote: 

![highlight output with the specified options](highlight-output.png)

Awesome output for such a simple command line tool!

## Go forth and highlight!
Highlight is straight-forward to install, easy to use, and ships with a great 
set of themes. Despite its simplicity, ```highlight``` is powerful and makes 
creating beautiful technical presentations a breeze. Try it out and see what you 
think!


[highlight]: http://www.andre-simon.de/doku/highlight/en/highlight.php
[homebrew]: http://brew.sh/
[install]: http://www.andre-simon.de/doku/highlight/en/install.php
[plugins]: http://www.andre-simon.de/doku/highlight/en/plugins.php
[windows]: http://www.andre-simon.de/zip/download.php
[cli-options]: http://www.andre-simon.de/doku/highlight/en/highlight.php