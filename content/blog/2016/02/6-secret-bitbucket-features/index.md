---
title: "6 Bitbucket secrets"
date: "2016-02-16T14:50:00+07:00"
author: "tpettersen"
categories: ["git", "bitbucket", "Atlassian Connect"]
---

<style>
  img.screenshot {
    margin: 10px auto;
    border: 1px #ccc solid;
  }

  .screenshot-wrapper {
    text-align: center;
  }

  img.omnibar {
    margin: 20px auto 10px auto;
  }
</style>

At Atlassian, one of our [design principles] is to *gracefully reveal depth*.
As we've iterated on our UX, certain Bitbucket power user features that
strayed too far from the happy path have been hidden away behind a dropdown or
keyboard shortcut. There they bide their time until an adventurous user stumbles
upon it through a capricious key press or mouse click (or someone reads the
docs). Here's six of my favourite Bitbucket Cloud features, that you've possibly
never heard of:

1. [Omnibar](#omnibar)
1. [Keyboard shortcuts](#keyboard-shortcuts)
1. [Binary snippets](#binary-snippets)
1. [Download a `zip` or `tar` of any commit](#download-a-zip-or-tar-of-any-commit)
1. [Customize the UI](#customize-the-ui)
1. [Enable pre-release features](#enable-pre-release-features)

# Omnibar

[Bitbucket]'s *omnibar* is a quick actions bar, similar to Jetbrains' `⇧⇧` or 
Sublime Text's `⌘+P`. From any page, you can launch the omnibar by pressing the 
`.` key.

By default, it shows you a set of context-sensitive actions:

<img class='screenshot' alt='repo actions' src='omni-actions.gif'
     title='Repository actions'>

Once you start typing, it matches on the names of repositories owned by you and
your team:

<div class='screenshot-wrapper'>
<img class='omnibar' alt='repositories' src='omni-repos.gif'
     title='Repository name matching'>
</div>

Plus the titles of your issues and pull requests (across all of your repositories):

<div class='screenshot-wrapper'>
<img class='omnibar' alt='issues and pull requests'
     src='omni-issues-and-prs.gif' title='Issue and pull request titles'>
</div>

And even filenames within the current repository:

<div class='screenshot-wrapper'>
<img class='omnibar' alt='filenames' src='omni-files.gif'
     title='Filename matching'>
</div>

If you happen to be a JIRA user as well, try hitting `.` the next time you're 
viewing a JIRA issue.

# Keyboard shortcuts

Using `.` to trigger the omnibar is just one of the many keyboard shortcuts
available. You can hit `⇧+?` on any page to see a context-sensitive list of
shortcuts. Some of the ones we use daily on the Bitbucket team are:

- **`/`** to focus the site search
- **`[`** to collapse the sidebar
- **`CTRL+↵`** to submit a comment

The modifiers for keys vary by browser and platform -- the above are Chrome on OS
X -- so bring up the shortcut list (`⇧+?`) in your own browser to check!

# Binary snippets

You might've used Bitbucket's [Snippets] to share code in the past, but you
might not be aware that you can also share other types of files with them. Just
drag any file onto the Snippet:

<img class='screenshot' alt='binary snippets' src='binary-snippets.gif'
     title='Binary snippets'>

This can include binary files like images, videos, or even compiled code! There
is a limit of 10 MB per file. There's also a
[handy command-line tool][bitbucket-snippet] for uploading files from your
shell.

<img class='screenshot' alt='bitbucket-snippet'
     src='https://developer.atlassian.com/blog/2015/11/scripting-with-node/progress-bar.gif'
     title='bitbucket-snippet CLI tool'>

# Download a `zip` or `tar` of any commit

The repository downloads page lets you download an archive of your code at a
particular commit with your choice of compression (`zip`, `tar.gz` or `tar.bz2`):

<img class='screenshot' alt='downloads page' src='downloads.png'
     title='Downloads'>

The URL that backs these links looks something like:

`bitbucket.org/atlassian/atlassian-connect-express/get/v1.0.4.zip`

Or more generally:

`bitbucket.org/<repo_owner>/<repo_name>/get/<revision>.<zip|tar.gz|tar.bz2>`

It turns out that you can replace `<revision>` with any non-ambiguous commit
identifier. So you could download a commit by specifying its short SHA-1:

`bitbucket.org/.../get/badc0de.zip`

Or use ancestry references to find, for example, the second great-grandparent
of the tip of your `master` branch:

`bitbucket.org/.../get/master^2~2.zip`

This can be quite handy for continuous integration and deployment scripts that
need to download a snapshot of your repository at a particular commit. There are
[all sorts of cool ways to specify commits in Git][revision selection], and the
`/get` end-point works for Mercurial repositories too!

# Customize the UI

Last June we launched the Bitbucket Connect framework, which allows you to
augment the Bitbucket UI. You may have used some of the
[third party developer tools][addon-directory] that have since launched
Bitbucket integrations using the framework. However you might not be aware that
you can use Bitbucket Connect to customize *your own* Bitbucket experience.

For example, this hunk of JSON is *actually* a freestanding Bitbucket Connect
add-on:

```javascript
{
    "key": "download-commit-as-zip",
    "name": "Download commit as ZIP",
    "description": "Adds a \"Download as ZIP\" link to the commits page on Bitbucket",
    "baseUrl": "https://bitbucket.org",
    "modules": {
        "webItems": [{
            "key": "download-link",
            "url": "https://bitbucket.org/{repo_path}/get/{commit_hash}.zip",
            "name": {
                "value": "Download as ZIP"
            },
            "location": "org.bitbucket.commit.summary.actions",
            "params": {
                "auiIcon": "aui-iconfont-down"
            }
        }],
        "oauthConsumer": {
            "clientId": "WaLh6mhKdRUDpVcXAH"
        }
    },
    "scopes": ["team"],
    "contexts": ["personal"]
}
```

Once installed, a **Download as ZIP** link is added to the Bitbucket commit
page, using the [`/get/<revision>.zip`](#download-a-zip-or-tar-of-any-commit)
end-point we looked at above:

<img class='screenshot' alt='download as zip' src='download-as-zip.png'
     title='Download as ZIP'>

The add-on is [hosted as a Snippet][download-as-zip] and can be installed using
this button:
<a class="aui-button aui-button-primary install-button"
   href="https://bitbucket.org/site/addons/authorize?descriptor_uri=https://bitbucket.org/!api/2.0/snippets/tpettersen/78Ajj/master/files/connect.json&redirect_uri=https://bitbucket.org/snippets/tpettersen/78Ajj">
   <span class="aui-icon aui-icon-small aui-iconfont-file-zip"></span>
   Install Download as Zip
</a>

You can use this technique to add links to your repositories, build
[visualizations for your code][run-bucket-run], or whole new features on top of
Bitbucket. For more information on building Bitbucket add-ons, check out the
[developer docs] or [this 30 minute video tutorial][lets-build]. Or feel free to
[bounce ideas off me](https://twitter.com/kannonboy).

# Enable pre-release features

If you've read this far, you're likely one of those special types who enjoy
living on the bleeding edge. If that sounds like you, head to
[Bitbucket Settings] and click **Manage features** in the left hand menu
to enable some pre-release features. This week, a slick new pull request
experience awaits!

<img class="screenshot" alt="features feature" src="features.png"
     title="the Features feature">

If early adoption is your thing, make sure you check back from time to time as
the Bitbucket team often put out teasers and betas well ahead of the official
release.

# That's it! For now.

Thanks for reading! If you have your own Bitbucket secrets you'd like to share --
or a nifty idea for a Bitbucket add-on -- please leave a comment below or hit me
up on Twitter! I'm [@kannonboy].

[design principles]: https://design.atlassian.com/how-we-design/principles/
[Bitbucket]: https://bitbucket.org
[revision selection]: https://git-scm.com/book/en/v2/Git-Tools-Revision-Selection
[Snippets]: https://bitbucket.org/snippets/new
[bitbucket-snippet]: https://www.npmjs.com/package/bitbucket-snippet
[addon-directory]: https://bitbucket.org/account/addon-directory
[download-as-zip]: https://bitbucket.org/snippets/tpettersen/78Ajj
[run-bucket-run]: https://bitbucket.org/tpettersen/run-bucket-run/src/master/game.html?at=master&fileviewer=run-bucket-run%3Agame
[developer docs]: https://developer.atlassian.com/bitbucket
[lets-build]: https://developer.atlassian.com/blog/2015/07/static-addon-for-bitbucket/
[Bitbucket Settings]: https://bitbucket.org/account/
[@kannonboy]: https://twitter.com/kannonboy

