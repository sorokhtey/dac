---
title: "The REST APIs you've been waiting for - The new JIRA Service Desk and JIRA Software REST APIs"
date: "2016-01-15T00:01:00+07:00"
author: "alui"
categories: ["jira"]
series: ["JIRA Startup Guides"]
---

We're delighted to announce that JIRA Service Desk now has a public REST API. This complements the JIRA Software REST API that we released with JIRA Software last year. These new APIs make it easier to quickly build integrations and Atlassian Connect add-ons for JIRA Software and JIRA Service Desk.

The JIRA Service Desk REST API is currently an experimental release, and will eventually become a stable release. The JIRA Software REST API is a stable release already. See our [REST API policy](https://developer.atlassian.com/display/HOME/Atlassian+REST+API+policy) for more details. Check out both REST APIs below:

* [JIRA Software REST API (latest)](https://docs.atlassian.com/jira-software/REST/cloud/)
* [JIRA Service Desk REST API](https://docs.atlassian.com/jira-servicedesk/REST/latest/)

You'll be happy to know that the JIRA Software REST API and JIRA Service Desk REST API have a range of resources for interacting with JIRA Software or JIRA Service Desk respectively. Although they don't have everything, we think we've covered the main scenarios in this initial release. Here's a few examples of what you can do with the new REST APIs:
* Get your JIRA Software add-on to retrieve the boards related to a project ([`GET /rest/agile/1.0/board`](https://docs.atlassian.com/greenhopper/REST/cloud/#agile/1.0/board-getAllBoards)), then create a sprint in one of the boards ([`POST /rest/agile/1.0/sprint`](https://docs.atlassian.com/greenhopper/REST/cloud/#agile/1.0/sprint-createSprint)).
* Build a custom JIRA Service Desk report, by getting all of the status ([`GET /rest/servicedeskapi/request/{issueIdOrKey}/status`](https://docs.atlassian.com/jira-servicedesk/REST/latest/#servicedeskapi/request/{issueIdOrKey}/status-getCustomerRequestStatus)) and SLA ([`GET /rest/servicedeskapi/request/{issueIdOrKey}/sla`](https://docs.atlassian.com/jira-servicedesk/REST/latest/#servicedeskapi/request/{issueIdOrKey}/sla-getSlaInformation)) information for customer requests.
* Allow another application to create a customer request ([`POST /rest/servicedeskapi/request`](https://docs.atlassian.com/jira-servicedesk/REST/latest/#servicedeskapi/request-createCustomerRequest)) in a service desk.

You can find all of the reference information you need in the REST API documentation linked above. Both REST APIs use the new documentation theme which makes them easier to scan, with expandable sections for resources. We've also written a [guide to exploring the JIRA Service Desk domain model via the REST API](https://developer.atlassian.com/display/JIRADEV/Guide+-+Exploring+the+JIRA+Service+Desk+domain+model+via+the+REST+APIs), which should be helpful to all Service Desk developers, new and old.

Happy integrating! If you need help with the new REST APIs, try posting a question on [Atlassian Answers](https://answers.atlassian.com/questions/topics/753681/jira-development) or log a ticket ([JIRA Software](https://jira.atlassian.com/browse/GHS) or [JIRA Service Desk](https://jira.atlassian.com/browse/JSD)).


<hr />
_Note, if you are currently using the private JIRA Agile REST resources, please read the following:_<br>
_**If you have an [Atlassian Connect](https://developer.atlassian.com/static/connect/docs/latest/index.html) add-on** -- Be aware that the resources that are marked as private in the [JIRA Software REST Scopes](https://developer.atlassian.com/static/connect/docs/latest/scopes/jira-software-rest-scopes.html) have been deprecated. These private resources will no longer be whitelisted on the 1st February 2016. You must migrate to the new public REST API before this date. Note, there is feature parity between the whitelisted private resources and the new public REST resources._<br>
_**If you have a P2 add-on** -- Be aware that the private REST resources that were whitelisted, have now been deprecated. You can continue to use these resources, but we recommend that you migrate to the new public REST API. Note, feature parity does not exist between these private resources and public REST API, and we cannot promise that there will be feature parity in future._
