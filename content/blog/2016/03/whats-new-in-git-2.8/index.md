---
title: "More on Git 2.8: push, grep, rebase, config neat things"
date: "2016-03-31T06:00:00+07:00"
author: "npaolucci"
categories: ["Docker","Git"]
---

Hear Hear, the new [Git 2.8.0 release][1] is out! In the past couple of weeks,
while the release candidate cycle was ongoing, I went through the commits and
release notes trying the new things and scribbling down the interesting bits.
To save you time here is a subjective selection of things to try out. Enjoy!

## `push -d` is a synonym of `push --delete`

This is a nice addition for both consistency and speed of typing. You could
already use `git branch -d` to delete a branch locally, and now you can shorten
remote branch deletion to `git push -d` as well. Example:

``` bash
git branch -d my-branch #deletes the branch locally if it's already been merged
git push -d origin my-branch #deletes the remote branch at origin
```

## `git grep`: Threading and recursive protection

A couple of relevant things have been integrated into `grep`'s functionality:

1. You can now specify how many threads `grep` should use while searching the
   working tree objects. Add `--num-threads` to the command line or add
   configuration flag `grep.threads` in `.gitconfig` to make it persistent.
   Example:

    ``` bash
        git grep --num-threads=5 <pattern>
    ```

2. If you ran `git grep` in a non-git folder, Git could run a huge recursive
   search up the folders which was not necessarily intended. In this latest
   version if you really want to use `grep` outside a Git repository, you have to
   explicitly provide the `--no-index` flag. This safety can be disabled via a
   flag by the name of `grep.fallbackToNoIndex`.

## `pull --rebase` supports interactive mode

`git pull --rebase` can now be run interactively as `git pull
--rebase=interactive`. This is an interesting addition to the `pull rebase`
workflow for when you want to quickly squash commits or edit the commit
messages at the time of the pull. Usual [rebase **warnings** apply][4].

## Ask `git config` anything ;)

Git config can now show where a value was set whether in a file, loaded from
standard input, a blob, or directly from the command line. For example I may
ask: "Where did I set my `st` (status) alias?" `git config` can now tell me:

``` bash
git config --show-origin alias.st

file:/Users/np/.gitconfig       status -s
```

## Other sparse tidbits

- Eye-candy progress bar for long running `git blame` was added.
- A new notation in the form of `<branch>^{/!-<pattern>}` is now available. It
  allows you to refer to a commit that is reachable from `<branch>` but does
  not match the given `<pattern>`.
- `git fetch` can be configured to use only `ipv4` (or `ipv6`). Invoke it as
  `git fetch -4` or `git fetch -6` respectively.
- Command pattern `git worktree add -B <branchname>` was not working as
  intended and is fixed now.

## Next steps

The above notes are just a selection, the release contains much more! For other
features included go to the source and see the [full release notes][1]. Also if
you happen to be in New York next week come join the Git community at [Git
Merge]!

Finally, if you want to keep in touch with me and hear of the latest things on Git and other tech endeavours, <a href="https://twitter.com/durdn" class="twitter-follow-button" data-show-count="false" data-size="large" data-dnt="true">Follow @durdn</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>


[@atlassiandev]: https://www.twitter.com/atlassiandev
[1]: https://git.kernel.org/cgit/git/git.git/plain/Documentation/RelNotes/2.8.0.txt
[3]: http://durdn.com/blog/2012/11/22/must-have-git-aliases-advanced-examples/
[Git Merge]: http://git-merge.com/
[4]: https://www.atlassian.com/git/tutorials/merging-vs-rebasing/the-golden-rule-of-rebasing
