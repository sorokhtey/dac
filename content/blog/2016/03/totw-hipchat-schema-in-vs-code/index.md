---
title: "Set up Visual Studio Code to build HipChat Connect add-ons faster"
author: "rwhitbeck"
date: "2016-03-07T16:00:00+07:00"
categories: ["totw", "atlassian-connect", "hipchat"]
lede: "Set up the HipChat JSON Schema in Visual Studio Code to make creating atlassian-connect.json
       files easier by using type ahead to suggest the keys available." 
---

This tip comes to us through a [pair programming session](https://www.youtube.com/watch?v=dYBjVTMUQY0) 
I did last week with [Rich Manalang](/blog/authors/rmanalang/), HipChat Developer Relations. 
We both love the new [Visual Studio Code editor](https://code.visualstudio.com/) as it has great 
debugging capabilities for Node.js. Rich showed me how to hook up the HipChat Atlassian Connect 
JSON Schema document into the editor. This made creating `atlassian-connect.json` descriptors fast 
and simple by utilizing IntelliSense.


I explain it all in this short screencast:

<iframe width="850" height="531" src="https://www.youtube.com/embed/taOFzMFovk4?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>

Here are the key things you'll need from the video to enhance your Visual Studio Code 
editor to use IntelliSense for creating HipChat `atlassian-connect.json` descriptors:

You can copy and paste this JSON code into your User Settings (don't forget to add 
a comma to the previous line):

``` javascript
"json.schemas": [
  {
    "fileMatch": [
      "/atlassian-connect.json"
    ],
    "url": "https://api.hipchat.com/v2/capabilities/addon/schema"
  }
]
```

Now whenever you're in a file named `atlassian-connect.json` you can use the `CTRL-SPACE` shortcut to bring up IntelliSense.

In the end, you'll be able to start building your HipChat Connect add-on at [ludicrous speed](https://www.youtube.com/watch?v=oApAdwuqtn8).

![Ludicrous speed, GO!](intellisense.gif)

Share your own tips and tricks in the comments or on Twitter, be sure to mention me: 
[@RedWolves](http://twitter.com/redwolves) or [@atlassiandev](http://twitter.com/atlassiandev)!

