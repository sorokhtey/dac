var _ = require('lodash');
var humanize = require('humanize-plus'); 

class JavaScriptAPI {
  constructor(apiData) {
    if (!apiData) {
      throw new Error('API data is missing.');
    }
    this.classes = getClasses(apiData);
    this.modules = getModules(apiData);
  }
}

function getModules(api) {
  let modules = [];

  if (!api || !api.modules) {
    throw new Error('Invalid API.');
  }

  api.modules.forEach((mod, index) => {
    mod.humanizeName = humanize.capitalize(mod.name);
    let thisModule = {       
      category: 'reference',
      type: 'jsdoc'
    };
    modules.push(_.extend(thisModule, mod));
  });

  return modules;
}

function getClasses(api) {
  let classes = [];

  if (!api || !api.classes) {
    throw new Error('Invalid API.');
  }

  api.classes.forEach((cls, index) => {
    let thisClass = {
      category: 'reference',
      type: 'jsdoc'
    };
    classes.push(_.extend(thisClass, cls));
  });

  return classes;
}

module.exports = JavaScriptAPI;