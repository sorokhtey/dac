#!/usr/bin/env bash

. scripts/enter-virtualenv.sh

mkdir -p test-reports
echo running actual tests
PYTHONPATH=`dirname $0`/../export py.test -v --junit-xml=test-reports/dac-tests.xml export

