import React, { PropTypes } from 'react'

const Button = ({text, onClick }) => {
  return (
    <a href="#" className="aui-button aui-button-primary"
       onClick={e => {
         e.preventDefault()
         onClick()
       }}
    >
      {text}
    </a>
  )
};

Button.propTypes = {
  onClick: PropTypes.func.isRequired
};

export default Button
