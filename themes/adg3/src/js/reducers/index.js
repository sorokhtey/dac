import {combineReducers} from "redux";
import {
    REQUEST_AID_PROFILE,
    RECEIVE_AID_PROFILE,
    REQUEST_LOGIN_AID_USER,
    REQUEST_BLOG_RSS,
    RECEIVE_BLOG_RSS
} from "../actions";

function profile(state = {
    isFetching: false,
    authenticated: false,
    didInvalidate: false,
    profile: {},
    openid_auth: {}
  }, action) {
  switch (action.type) {
    case REQUEST_AID_PROFILE:
      return Object.assign({}, state, {
        isFetching: true,
        didInvalidate: false
      })
    case RECEIVE_AID_PROFILE:
      return Object.assign({}, state, {
        isFetching: false,
        didInvalidate: false,
        authenticated: action.authenticated,
        profile: action.profile,
        lastUpdated: action.receivedAt
      })
    case REQUEST_LOGIN_AID_USER:
      return Object.assign({}, state, {
        didInvalidate: false
      })
    case RECEIVE_LOGIN_AID_USER:
      return Object.assign({}, state, {
        didInvalidate: false,
        openid_auth: action.openid_auth,
        lastUpdated: action.receivedAt
      })
    default:
      return state
  }
}

function rss(state, action) {
  switch (action.type) {
    case REQUEST_BLOG_RSS:
      return Object.assign({}, state, {
        isFetching: true,
        didInvalidate: false
      })
    case RECEIVE_BLOG_RSS:
      return Object.assign({}, state, {
        posts: action.posts || [],
        isFetching: false,
        didInvalidate: true,
        lastUpdated: action.receivedAt
      })
    default:
      return {
        isFetching: false,
        didInvalidate: false,
        posts: []
      }
  }
}

const rootReducer = combineReducers({
  rss
})

export default rootReducer
